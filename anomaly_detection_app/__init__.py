# app/__init__.py

import logging

from flask import Blueprint
from flask_restplus import Api

from .main.controller.models_controller import api as models_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='Detección de Anomalías en Procesos de contratación',
          version='1.0'
          )

api.add_namespace(models_ns, path='/modelos')

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    handlers=[
        logging.StreamHandler()
    ])
