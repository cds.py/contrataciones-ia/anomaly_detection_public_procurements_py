from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask

from anomaly_detection_app.main.core.transform.cotizacion import get_from_bcp_service, update_cotizaciones
from .config import config_by_name, COTIZACIONES_SCHEDULER_TIME
from .database import db


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    db.init_app(app)
    with app.app_context():
        scheduler = BackgroundScheduler()
        scheduler.add_job(update_cotizaciones, trigger='interval', seconds=COTIZACIONES_SCHEDULER_TIME)
        scheduler.start()
        try:
            # To keep the main thread alive
            return app
        except:
            # shutdown if app occurs except
            scheduler.shutdown()

