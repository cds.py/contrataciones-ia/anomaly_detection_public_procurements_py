import datetime
import logging
import os
import traceback

from joblib import dump
from sklearn.ensemble import IsolationForest
from sklearn.tree import DecisionTreeClassifier
from uwsgidecorators import thread

from anomaly_detection_app.main import config
from anomaly_detection_app.main.config import MODELS_DATA_PATH, variables_categoricas
from anomaly_detection_app.main.core.transform.transform import get_and_transform_bulk
from anomaly_detection_app.main.core.util import prepare_decision_tree_data, preparar_datos_entrenamiento, \
    guardar_predicciones, change_logger, remove_logger_file_handler
from anomaly_detection_app.main.models.entrenamiento import Entrenamiento, Etapas, EntrenamientoEstado

PATH = os.path.dirname(os.path.abspath(__file__))


@thread
def entrenar(entrenamiento, anio_inicio, anio_fin, etapa):
    log_path = os.path.join(config.LOG_PATH,
                            'training-{}.log'.format(datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S")))
    change_logger(log_path)
    logging.info("Entrenando con datos de {} a {}, etapa={}".format(anio_inicio, anio_fin, etapa))
    entrenamiento.save()
    try:
        sample_size = 20
        if etapa == Etapas.CONVOCATORIA.name:
            max_feature = 14
            forest_size = 100
        else:
            max_feature = 26
            forest_size = 100

        logging.info('Obteniendo y transformando datos para entrenamiento')
        csv_file = get_and_transform_bulk(anio_inicio, anio_fin, etapa)
        entrenamiento.datos_path = csv_file
        entrenamiento.save()
        contract_data, input_contract_data = preparar_datos_entrenamiento(csv_file)

        # Guardamos las marcas para usarlas en el árbol de decisión
        marcas = contract_data['marcas_sin_transformar']
        modalidades = contract_data['modalidad_descripcion']
        categorias = contract_data['categoria_descripcion']
        formas_adjudicacion = contract_data['forma_adjudicacion']

        # Las quitamos para el IsolationForest
        contract_data = contract_data.drop(variables_categoricas, axis=1)
        file_name = get_file_name(etapa, anio_inicio, anio_fin)
        logging.info('Entrenando')
        clf = IsolationForest(n_estimators=forest_size, max_samples=sample_size, behaviour='new',
                              random_state=0, n_jobs=1, max_features=max_feature, contamination='auto')
        clf.fit(contract_data, y=None, sample_weight=None)
        # guardamos el archivo con las predicciones

        training_file = os.path.join(MODELS_DATA_PATH, 'modelo-clasificacion-'+file_name)
        logging.info('guardando modelo entrenado')
        dump(clf, training_file)
        entrenamiento.modelo_clasificacion_path = training_file
        entrenamiento.save()
        prediction_file, contract_data['score'] = guardar_predicciones(clf, contract_data, input_contract_data,
                                                                       training_file.replace('joblib', 'csv'))

        logging.info('Generando árbol de decisión')
        # Agregamos los datos para usar en el decision tree
        contract_data['marcas'] = marcas
        contract_data['modalidad_descripcion'] = modalidades
        contract_data['categoria_descripcion'] = categorias
        contract_data['forma_adjudicacion'] = formas_adjudicacion

        tree_file = get_decision_tree(contract_data, etapa, anio_inicio, anio_fin)
        entrenamiento.modelo_interpretacion_path = tree_file
        entrenamiento.datos_path = prediction_file
        entrenamiento.fecha_fin = datetime.datetime.now()
        entrenamiento.estado = EntrenamientoEstado.ENTRENADO.name
        entrenamiento.save()
        logging.info('Entrenamiento exitoso')
        remove_logger_file_handler()
    except Exception as e:
        logging.info('error al entrenar', e)
        traceback.print_exc()
        entrenamiento.estado = EntrenamientoEstado.ERROR.name
        entrenamiento.fecha_fin = datetime.datetime.now()
        entrenamiento.save()
        remove_logger_file_handler()


def get_file_name(etapa, anio_inicio, anio_fin):
    return '{}-{}-{}-{}.joblib'.format(etapa, anio_inicio, anio_fin,
                                       datetime.datetime.today().strftime("%Y-%m-%d-%H:%M:%S"))


def get_decision_tree(data, etapa, anio_inicio, anio_fin):
    data = prepare_decision_tree_data(etapa, data)
    y_train = data['score_label'].to_numpy()
    x_train = data.drop(['score_label'], axis=1).to_numpy()
    estimator = DecisionTreeClassifier(random_state=0)
    estimator.fit(x_train, y_train)
    file_name = get_file_name(etapa, anio_inicio, anio_fin)
    training_file = os.path.join(MODELS_DATA_PATH, 'modelo-interpretacion-' + file_name)
    dump(estimator, training_file)
    return training_file
