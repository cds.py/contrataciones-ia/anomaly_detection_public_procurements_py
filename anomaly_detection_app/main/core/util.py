import logging

import pandas as pd

from anomaly_detection_app.main.config import llamados_columnas_en_orden, contratos_columnas_en_orden, marcas, \
    decision_tree_drop_columns, QUARTIL_1, categorias, modalidades, formas_adjudicacion, variables_categoricas
from anomaly_detection_app.main.models.entrenamiento import Etapas


def score_a_nominal(row):

    if row['score'] > 0:
        return 0
    elif QUARTIL_1 <= row['score'] <= 0:
        return 1
    else:
        return 2


def nominal_a_score(score):
    if score > 0:
        return 'green'
    elif QUARTIL_1 <= score <= 0:
        return 'yellow'
    else:
        return 'red'


def get_marca(marca_label, marca_row):
    return 1 if marca_label in str(marca_row) else 0


def prepare_decision_tree_data(etapa, data):
    columnas_categoricas = marcas + categorias + modalidades + formas_adjudicacion + ['score_label']
    llamado_columnas = llamados_columnas_en_orden + columnas_categoricas
    llamado_columnas = [item for item in llamado_columnas if item not in decision_tree_drop_columns]
    contrato_columnas = contratos_columnas_en_orden + columnas_categoricas + ['score_label']
    contrato_columnas = [item for item in contrato_columnas if item not in decision_tree_drop_columns]
    data['score_label'] = data.apply(lambda row: score_a_nominal(row), axis=1)
    data = data.drop(['score'], axis=1)

    # Con esto se convierte cada una de estos valores categóricos
    # en una columna para poder interpretarlo correctamente en el
    # árbol de decisión. Se crea una columna por marca, categoría, modalidad y forma de adjudicacion posible.
    # Si el valor corresponde a la columna entonces se completa con 1 si no con 0
    for marca in marcas:
        data[marca] = data.apply(lambda row: get_marca(marca, row['marcas']), axis=1)
    for categoria in categorias:
        data[categoria] = data.apply(lambda row: get_marca(categoria, row['categoria_descripcion']), axis=1)
    for modalidad in modalidades:
        data[modalidad] = data.apply(lambda row: get_marca(modalidad, row['modalidad_descripcion']), axis=1)
    for forma in formas_adjudicacion:
        data[forma] = data.apply(lambda row: get_marca(forma, row['forma_adjudicacion']), axis=1)

    # Luego se borran las columnas numéricas que representan estos valores ca

    if etapa == Etapas.CONTRATO.name:
        data = data[contrato_columnas]
    else:
        data = data[llamado_columnas]
    data = data.drop(decision_tree_drop_columns + variables_categoricas + ['marcas'], axis=1, errors='ignore')
    return data


def guardar_predicciones(clf, contract_data, input_contract_data, file_name):
    logging.info('Prediciendo con el modelo entrenado')
    prediction = clf.predict(contract_data)
    scores = clf.decision_function(contract_data)
    logging.info('Guardando predicciones')
    predictions = pd.DataFrame()
    predictions["predicted_class"] = prediction
    predictions["score"] = scores
    predictions["slug"] = input_contract_data["slug"]
    headers = ['predicted_class', 'Scores', 'slug']
    prediction_file = file_name
    predictions.to_csv(prediction_file,
                       header=headers, index=None, sep=',', mode='w')
    return prediction_file, scores


def preparar_datos_entrenamiento(csv_file):
    input_contract_data = pd.read_csv(csv_file, sep=',')
    contract_data = input_contract_data.drop(['slug'], axis=1)
    contract_data.replace(to_replace='None', value=0, inplace=True)
    contract_data.replace(to_replace='NaN', value=0, inplace=True)
    contract_data.fillna(0, inplace=True)
    return contract_data, input_contract_data


def change_logger(filename):
    """
    Método para cambiar el logger a un archivo nuevo según el path pasado como parámetro. Este método es usado
    al realizar un nuevo entrenamiento, así cada entrenamiento tiene su propio archivo de log
    """
    filehandler = logging.FileHandler(filename)
    formatter = logging.Formatter('%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s')
    filehandler.setFormatter(formatter)
    log = logging.getLogger()
    for hdlr in log.handlers[:]:  # remove the existing file handlers
        if isinstance(hdlr, logging.FileHandler):
            log.removeHandler(hdlr)
    log.addHandler(filehandler)  # set the new handler


def remove_logger_file_handler():
    log = logging.getLogger()
    for hdlr in log.handlers[:]:  # remove the existing file handlers
        if isinstance(hdlr, logging.FileHandler):
            log.removeHandler(hdlr)
