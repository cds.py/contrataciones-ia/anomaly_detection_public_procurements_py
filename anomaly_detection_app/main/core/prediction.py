import logging
import os

from joblib import load

from anomaly_detection_app.main import config
from anomaly_detection_app.main.config import variables_categoricas
from anomaly_detection_app.main.core.transform.transform import get_and_transform_convocatoria, \
    get_and_transform_contrato
from anomaly_detection_app.main.core.util import prepare_decision_tree_data, nominal_a_score
from anomaly_detection_app.main.models.entrenamiento import Entrenamiento, Etapas

PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), config.MODELS_FOLDER_NAME)


def predict_convocatoria(convocatoria_slug):
    tender_tradicional = get_and_transform_convocatoria(convocatoria_slug)
    if tender_tradicional is None:
        logging.info('Datos no encontrados')
        return None
    a_ret = {'convocatoria_slug': convocatoria_slug}
    a_ret = prediction_to_ret(Etapas.CONVOCATORIA.name, tender_tradicional, a_ret)
    return a_ret


def predict_contrato(contrato_slug):
    contrato_tradicional = get_and_transform_contrato(contrato_slug)
    if contrato_tradicional is None:
        logging.info('Datos no encontrados')
        return None
    a_ret = {'contrato_slug': contrato_slug}
    a_ret = prediction_to_ret(Etapas.CONTRATO.name, contrato_tradicional, a_ret)
    return a_ret


def prediction_to_ret(etapa, data, a_ret):
    modelo_interpretacion = Entrenamiento.get_activo_entrenado_etapa(etapa, interpretacion=True)
    modelo_clasificacion = Entrenamiento.get_activo_entrenado_etapa(etapa)
    clf = load(modelo_clasificacion)
    marcas = data['marcas_sin_transformar']
    modalidades = data['modalidad_descripcion']
    categorias = data['categoria_descripcion']
    formas_adjudicacion = data['forma_adjudicacion']

    # Las quitamos para el IsolationForest
    data = data.drop(variables_categoricas + ['slug'], axis=1)
    scores = clf.decision_function(data)[0]
    data['marcas'] = marcas
    data['modalidad_descripcion'] = modalidades
    data['categoria_descripcion'] = categorias
    data['forma_adjudicacion'] = formas_adjudicacion
    data['score'] = scores
    a_ret['score'] = scores
    data['marcas'] = marcas,
    a_ret['rules'] = get_decision_path(etapa, data, modelo_interpretacion)
    a_ret['model'] = modelo_clasificacion
    a_ret['risk'] = nominal_a_score(scores)
    return a_ret


def get_decision_path(etapa, data, modelo_interpretacion):
    data = data.drop(['slug'], axis=1, errors='ignore')
    estimator = load(modelo_interpretacion)
    data = prepare_decision_tree_data(etapa, data)
    x_train = data.drop(['score_label'], axis=1).to_numpy()
    column_names = list(data.columns)
    feature = estimator.tree_.feature
    threshold = estimator.tree_.threshold
    node_indicator = estimator.decision_path(x_train)
    leave_id = estimator.apply(x_train)
    sample_id = 0
    node_index = node_indicator.indices[node_indicator.indptr[sample_id]:node_indicator.indptr[sample_id + 1]]
    a_ret = []
    for node_id in node_index:
        if leave_id[sample_id] == node_id:
            continue

        if x_train[sample_id, feature[node_id]] <= threshold[node_id]:
            threshold_sign = "<="
        else:
            threshold_sign = ">"
        a_ret.append("%s (= %s) %s %s)" % (column_names[feature[node_id]],
                                           x_train[sample_id, feature[node_id]], threshold_sign,
                                           threshold[node_id]))
    return a_ret
