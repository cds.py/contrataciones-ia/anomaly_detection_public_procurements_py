"""
Métodos relacionados a la conversión de textos.
"""

import hashlib

from anomaly_detection_app.main.core.transform.utils import diccionario


def convert_array(value, dict_name):
    if not value:
        return 0
    val = []
    for i in diccionario.dictionary[dict_name]:
        if i in value:
            val.append(1)
        else:
            val.append(0)
    i = 0
    for bit in val:
        i = (i << 1) | bit
    return i if i is not None else 0


def hash_value(text, name, hash, ruc=False):
    if ruc:
        text = remove_dividing_characters(text)
    if text:
        if hash:
            hash_md5 = hashlib.md5(text.encode('utf-8'))
            hex_digest = hash_md5.hexdigest()
            key = int(hex_digest, 16)
            key = int(str(key)[0:18])
        else:
            key = text
        if name:
            if key in diccionario.name_diccionario.keys() & name != diccionario.name_diccionario[key]:
                raise Exception('value in dict already exists, old value: ' + diccionario.name_diccionario[
                    key] + ' newValue: ' + name)
            elif key not in diccionario.name_diccionario.keys():
                diccionario.name_diccionario[key] = name
        else:
            if key in diccionario.name_diccionario.keys() & text != diccionario.name_diccionario[key]:
                raise Exception('value in dict already exists, old value: ' + diccionario.name_diccionario[
                    key] + ' newValue: ' + text)
            elif key not in diccionario.name_diccionario.keys():
                diccionario.name_diccionario[key] = text
    else:
        key = None
    return key


def remove_dividing_characters(text):
    new = ''.join(c for c in text if c.isdigit())
    if new == '':
        new = None
    else:
        new = int(new)
    return new


def append_to_list(title_row, list_data, indice, funcion):
    for i in list_data.keys():
        if len(list_data[i]) < len(title_row):
            if funcion == 'append':
                list_data[i].append(None)
            elif funcion == 'insert':
                list_data[i].insert(indice, None)


def depth(d, level=1):
    if not isinstance(d, dict) or not d:
        return level
    return max(depth(d[k], level + 1) for k in d)