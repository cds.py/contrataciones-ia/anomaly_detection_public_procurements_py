"""
Métodos relacionados a la transformación de datos de entidades o proveedores
"""


def limpiar_nombre_ciudad(ciudad, departamento, pais):
    """
    Método que estandariza el nombre de la ciudad recibida como parámetro según los casos vistos
    :param ciudad:
    :param departamento:
    :param pais:
    :return: ciudad con nombre correcto
    """
    if ciudad is not None:
        ciudad = ciudad.upper().replace('-', '').replace('(MUNICIPIO)', '').replace("( MUNICIPIO )", '').replace('.', ''). replace('(DISTRITO)', '')
    if departamento is not None:
        departamento = departamento.upper().strip().replace('-', '')
    if pais is not None:
        pais = pais.upper().strip().replace('-', '')

    if departamento is not None:
        if departamento == "ASUNCIÓN":
            ciudad = "ASUNCIÓN"
            departamento = 'CENTRAL'
            pais = 'PARAGUAY'
    if ciudad is not None:
        if "ASUN" in ciudad:
            if departamento == 'CENTRAL' or pais == 'PARAGUAY':
                ciudad = "ASUNCIÓN"
        elif "EMBY" in ciudad:
            ciudad = "ÑEMBY"
        elif 'FILA' in ciudad or 'FERN' in ciudad:
            ciudad = 'FILADELFIA'
        elif "ACUNCION" in ciudad:
            ciudad = "ASUNCIÓN"
        elif "AREG" in ciudad:
            ciudad = "AREGUÁ"
        elif "AUSNCION" in ciudad:
            ciudad = "ASUNCIÓN"
        elif "BELLA VISTA" in ciudad:
            ciudad = "BELLA VISTA"
        elif "AYOLAS" in ciudad:
            ciudad = "AYOLAS"
        elif "BENJAMIN ACEVAL" in ciudad:
            ciudad = "BENJAMIN ACEVAL"
        elif "CAAC" in ciudad:
            ciudad = "CAACUPÉ"
        elif "CAAG" in ciudad:
            ciudad = "CAAGUAZÚ"
        elif "CAAZ" in ciudad:
            ciudad = "CAAZAPÁ"
        elif "CAPIA" in ciudad:
            ciudad = "CAPIATÁ"
        elif "CAPII" in ciudad:
            ciudad = "CAPIIBARY"
        elif "CAPITAN MIRANDA" in ciudad:
            ciudad = "CAPITÁN MIRANDA"
        elif "CARAP" in ciudad:
            ciudad = "CARAPEGUÁ"
        elif "CARLOS" in ciudad:
            ciudad = "CARLOS ANTONIO LÓPEZ"
        elif "CDE" in ciudad or "CIUDAD DEL ESTE" in ciudad or "CUIDAD DEL ESTE" in ciudad or "DEL ESTE" in ciudad:
            ciudad = "CIUDAD DEL ESTE"
        elif "CHOR" in ciudad:
            ciudad = "CHORÉ"
        elif "BUENOS AIRES" in ciudad:
            ciudad = "BUENOS AIRES"
        elif "OVIEDO" in ciudad and "RAUL" not in ciudad:
            ciudad = "CORONEL OVIEDO"
        elif "COLONIA IRU" in ciudad:
            ciudad = "COLONIA IRUÑA"
        elif "CONC" in ciudad:
            ciudad = "CONCEPCIÓN"
        elif "CNEL MARTINEZ" in ciudad:
            ciudad = "CORONEL MARTINEZ"
        elif "CORRIENTE" in ciudad:
            ciudad = "CORRIENTES"
        elif "CURITIBA" in ciudad:
            ciudad = "CURITIBA"
        elif "MBARACAY" in ciudad:
            ciudad = "MBARACAYÚ"
        elif "DOMINGO M" in ciudad:
            ciudad = "DOMINGO MARTINEZ DE IRALA"
        elif "AYALA" in ciudad:
            ciudad = "EUSEBIO AYALA"
        elif "EDEL" in ciudad:
            ciudad = "EDELIRA"
        elif "ENCAR" in ciudad:
            ciudad = "ENCARNACIÓN"
        elif "DE LA MORA" in ciudad or "FERNANDO DE MORA" in ciudad:
            ciudad = "FERNANDO DE LA MORA"
        elif "FRANCISCO CABALLERO" in ciudad:
            ciudad = "FRANCISCO CABALLERO ÁLVAREZ"
        elif "GRAL AR" in ciudad:
            ciudad = "GENERAL ARTIGAS"
        elif "GRAL DELGADO" in ciudad:
            ciudad = "GENERAL DELGADO"
        elif "GUAYA" in ciudad or "GUAJ" in ciudad:
            ciudad = "GUAYAIBÍ"
        elif "HERNA" in ciudad:
            ciudad = "HERNANDARIAS"
        elif "HOHENAU" in ciudad:
            ciudad = "HOHENAU"
        elif "ITA" in ciudad:
            ciudad = "ITÁ"
        elif "ITACDEL ROSARIO" in ciudad:
            ciudad = "ITACURUBI DEL ROSARIO"
        elif "ITACURUBI DE LA CORDILLERA" in ciudad:
            ciudad = "ITACURUBÍ DE LA CORDILLERA"
        elif "ITAU" in ciudad or "ITAG" in ciudad:
            ciudad = "ITAUGUÁ"
        elif "ITAPA" in ciudad or "ITAPÁ" in ciudad:
            ciudad = "ITAPÚA"
        elif "AUGUSTO SALDIVAR" in ciudad or "JA SALDIVAR" in ciudad or "JASALDIVAR" in ciudad:
            ciudad = "J AUGUSTO SALDÍVAR"
        elif "J E ESTIGARRIBIA" in ciudad or "JE ESTIGARRIBIA" in ciudad or "JEULOGIO ESTIGARRIBIA" in ciudad:
            ciudad = "J EULOGIO ESTIGARRIBIA"
        elif "OCAMPOS" in ciudad:
            ciudad = "JOSÉ DOMINGO OCAMPOS"
        elif "LEARY" in ciudad:
            ciudad = "JUAN E O LEARY"
        elif "MALLORQUIN" in ciudad:
            ciudad = "JUAN LEÓN MALLORQUÍN"
        elif "OTAÑO" in ciudad:
            ciudad = "JULIO D OTAÑO"
        elif "LA PALOMA" in ciudad:
            ciudad = "LA PALOMA"
        elif "LAM" in ciudad:
            ciudad = "LAMBARÉ"
        elif "LUMPIO" in ciudad:
            ciudad = "LIMPIO"
        elif "MARIA AUXILIADORA" in ciudad:
            ciudad = "MARÍA AUXILIADORA"
        elif "MARIANO" in ciudad or "MRALONSO" in ciudad:
            ciudad = "MARIANO ROQUE ALONSO"
        elif "MAYOR JDMARTINEZ" in ciudad:
            ciudad = "MAYOR MARTINEZ"
        elif "MCAL" in ciudad or "ESTIGARRIBIA" in ciudad:
            ciudad = "MARISCAL ESTIGARRIBIA"
        elif "MINGA G" in ciudad:
            ciudad = "MINGA GUAZÚ"
        elif "MONTEVIDEO" in ciudad:
            ciudad = "MONTEVIDEO"
        elif "MUMBAI" in ciudad:
            ciudad = "MUMBAI"
        elif "NUEVA ESPERANZA" in ciudad:
            ciudad = "NUEVA ESPERANZA"
        elif "NULL" in ciudad:
            ciudad = "DESCONOCIDO"
        elif "PARAGAUYRI" in ciudad:
            ciudad = "PARAGUARÍ"
        elif "JUAN CABALLERO" in ciudad or "PEDRO JUANCABALLERO" in ciudad:
            ciudad = "PEDRO JUAN CABALLERO"
        elif "PDTEFRANCO" in ciudad or "PDTE FRANCO" in ciudad or "PDTFRANCO" in ciudad or "PDT FRANCO" in ciudad:
            ciudad = "PRESIDENTE FRANCO"
        elif "PENDIENTE ACTUALIZACION DATOS" in ciudad:
            ciudad = "DESCONOCIDO"
        elif "PILAE" in ciudad:
            ciudad = "PILAR"
        elif "RAUL A OVIEDO" in ciudad:
            ciudad = "RAUL ARSENIO OVIEDO"
        elif "SALTO" in ciudad:
            ciudad = "SALTO DEL GUAIRÁ"
        elif "SAN COSME Y DAMIAN" in ciudad:
            ciudad = "SAN COSME Y DAMIÁN"
        elif "SAN IGNACIO" in ciudad:
            ciudad = "SAN IGNACIO"
        elif "NEPOMUCENO" in ciudad:
            ciudad = "SAN JUAN NEPOMUCENO"
        elif "BAUTISTA" in ciudad:
            ciudad = "SAN JUAN BAUTISTA"
        elif "LAZARO" in ciudad:
            ciudad = "SAN LÁZARO"
        elif "LORENZO" in ciudad or "LORENSO" in ciudad:
            ciudad = "SAN LORENZO"
        elif "SAN MIGUEL" in ciudad:
            ciudad = "SAN MIGUEL"
        elif "SAN PEDRO" in ciudad and departamento is not None and departamento == "SAN PEDRO":
            ciudad = "SAN PEDRO DE YCUAMANDYYU"
        elif "SAN PEDRO DEL PARANÁ" in ciudad and departamento is not None and departamento == "ITAPÚA":
            ciudad = "ENCARNACIÓN"
        elif "SAN RAFAEL DEL PARANA" in ciudad:
            ciudad = "SAN RAFAEL DEL PARANÁ"
        elif "SAN ROQUE GONZALEZ" in ciudad:
            ciudad = "SAN ROQUE GONZÁLEZ DE SANTA CRUZ"
        elif "SANTA ROSA DEL MODAY" in ciudad:
            ciudad = "SANTA ROSA DEL MONDAY"
        elif "SANTIAGO" in ciudad and departamento is not None and 'MISIONES' in departamento:
            ciudad = "SANTIAGO MISIONES"
        elif "TOBAT" in ciudad:
            ciudad = "TOBATÍ"
        elif "TOMAS ROMERO PEREIRA" in ciudad:
            ciudad = "TOMÁS ROMERO PEREIRA"
        elif "MANUEL IRALA FERNANDEZ" in ciudad:
            ciudad = "TTE MANUEL IRALA FERNANDEZ"
        elif "VALLE" in ciudad:
            ciudad = "VALLEMÍ"
        elif "VILLA ELISA" in ciudad or "VILLAELISA" in ciudad or "VILLA ELIZA" in ciudad or "VILLAELIZA" in ciudad:
            ciudad = "VILLA ELISA"
        elif "VILLARRICA" in ciudad or "VILARRICA" in ciudad or "VILLARICA" in ciudad:
            ciudad = "VILLARRICA"
        elif "YAGUARON" in ciudad:
            ciudad = "YAGUARÓN"
        elif "YBY" in ciudad:
            ciudad = "YBY YAÚ"
        elif "YPACARA" in ciudad:
            ciudad = "YPACARAÍ"
        elif "YPANE" in ciudad:
            ciudad = "YPANÉ"

    return ciudad
