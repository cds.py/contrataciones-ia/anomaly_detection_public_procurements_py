import logging
from datetime import datetime, timedelta

import requests
from bs4 import BeautifulSoup
from urllib3.exceptions import ReadTimeoutError

from anomaly_detection_app.main.config import BCP_PAGE_URL
from anomaly_detection_app.main.util import execute_query


def get_exchange_rate(moneda, date):
    if moneda == 'USD':
        date = date.date()
        datos = execute_query('SELECT compra from sicp.cotizacion where fecha = %s;', [str(date)], True)
        if not datos:
            return get_from_bcp_service(date)
        return float(datos[0])
    elif moneda == 'PYG':
        return 1


def insert_exchange_rate(fecha, compra, venta):
    execute_query('INSERT INTO sicp.cotizacion (fecha, compra, venta) VALUES(%s, %s, %s)',
                  (fecha, compra, venta), False)


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


def update_cotizaciones():
    """
    Método a ejecutar 1 vez al día para obtener la cotización del día
    """
    last_date = execute_query('SELECT fecha from sicp.cotizacion order by fecha desc;', None, True)[0]
    logging.info("Última fecha cargada en la base de datos: {}".format(last_date))
    # mas 1 porque range no es inclusive
    today = datetime.today() + timedelta(1)
    for single_date in daterange(last_date, today):
        get_from_bcp_service(single_date)


def get_from_bcp_service(fecha=None):
    """
    Método para scrapear el sitio del BCP y obtener la cotización de la fecha pasada como parámetro
    """
    if not fecha:
        fecha = datetime.today()
    logging.info('Cotizacion del día no existe, buscando en BCP')
    fecha_a_buscar = fecha.strftime('%d/%m/%Y')
    try:
        soup = BeautifulSoup(
            requests.post(
                BCP_PAGE_URL,
                timeout=30,
                headers={'user-agent': 'Mozilla/5.0'},
                verify=False,
                data={'fecha': fecha_a_buscar}

            ).text,
            'html.parser',
        )


        compra_array = soup.find(
            class_='table table-striped table-bordered table-condensed'
        ).select('tr > td:nth-of-type(2)')
        venta_array = soup.find(
            class_='table table-striped table-bordered table-condensed'
        ).select('tr > td:nth-of-type(3)')
        posicion = len(compra_array) - 1
        compra = float(compra_array[posicion].get_text().replace('.', '').replace(',', '.').strip())
        venta = float(venta_array[posicion].get_text().replace('.', '').replace(',', '.').strip())
        logging.info('Cotizacion del día {}: {}'.format(fecha_a_buscar, compra))
        insert_exchange_rate(fecha, compra, venta)
    except Exception:
        compra = 1
        logging.error('Timeout al obtener la cotizacion del BCP para fecha', fecha_a_buscar)
    return compra

