import datetime
import logging
import os

import pandas
from dateutil.relativedelta import relativedelta

from anomaly_detection_app.main import util
from anomaly_detection_app.main.config import TRAINING_DATA_PATH, llamados_columnas_en_orden, \
    contratos_columnas_en_orden
from anomaly_detection_app.main.core.transform.cotizacion import get_exchange_rate
from anomaly_detection_app.main.core.transform.queries import QUERIES_PATH
from anomaly_detection_app.main.core.transform.utils.convert_string import convert_array, hash_value
from anomaly_detection_app.main.core.transform.utils.diccionario import initialize_dict
from anomaly_detection_app.main.core.transform.utils.parties import limpiar_nombre_ciudad
from anomaly_detection_app.main.models.entrenamiento import Etapas

CONTRACTS_QUERY_FILE = os.path.join(QUERIES_PATH, 'contracts.sql')
TENDER_QUERY_FILE = os.path.join(QUERIES_PATH, 'tender.sql')


def get_and_transform_convocatoria(convocatoria_slug):
    dic_dir = 'tmp.json'
    initialize_dict(dic_dir)
    connection = None
    try:
        connection = util.get_connection()
        where = " lla.slug = '%s' " % convocatoria_slug
        with open(TENDER_QUERY_FILE, 'r') as query:
            logging.info("Ejecutando query {}".format(TENDER_QUERY_FILE))
            results = pandas.read_sql_query(query.read().format(where),
                                            connection)
            if not results.empty:
                return transform(results, Etapas.CONVOCATORIA.name)
    finally:
        if connection:
            connection.close()
    return None


def get_and_transform_contrato(contrato_slug):
    dic_dir = 'tmp.json'
    initialize_dict(dic_dir)
    connection = None
    try:
        connection = util.get_connection()
        file_name = CONTRACTS_QUERY_FILE
        where = " pa.slug = '%s' " % contrato_slug
        with open(file_name, 'r') as query:
            logging.info("Ejecuntado query {}".format(file_name))
            results = pandas.read_sql_query(query.read().format(where),
                                            connection)
            if not results.empty:
                return transform(results, Etapas.CONTRATO.name)
    finally:
        if connection:
            connection.close()
    return None


def get_and_transform_bulk(anio_inicio, anio_fin, etapa):
    dic_dir = 'tmp.json'
    initialize_dict(dic_dir)
    connection = None
    file_name = '{}-{}-{}-{}.csv'.format(etapa, anio_inicio, anio_fin,
                                         datetime.datetime.today().strftime("%Y-%m-%d-%H:%M:%S"))
    logging.info("Preparando CSV {}".format(file_name))
    try:
        connection = util.get_connection()
        query_file_name = TENDER_QUERY_FILE
        if etapa == Etapas.CONTRATO.name:
            query_file_name = CONTRACTS_QUERY_FILE
        where = ' pac.anio between %d and %d ' % (anio_inicio, anio_fin)
        with open(query_file_name, 'r') as query:
            results = pandas.read_sql_query(query.read().format(where), connection,
                                            chunksize=1000)
            to_csv = []
            logging.info("Transformando")
            for result in results:
                transformed = transform(result, etapa)
                to_csv.append(transformed)
            pandas.concat(to_csv).to_csv(os.path.join(TRAINING_DATA_PATH, file_name), index=False)
            return os.path.join(TRAINING_DATA_PATH, file_name)

    finally:
        if connection:
            connection.close()


def convertir_marcas(x, tipo):
    return convert_array(x.split(';'), tipo) if x else 0


def monto(monto, moneda, fecha):
    if not monto or not moneda:
        return 0
    if isinstance(fecha, str):
        fecha = util.convert_fecha(fecha)
    return monto * get_exchange_rate(moneda, fecha)


def convert_fecha(fecha):
    if fecha:
        return util.convert_fecha(fecha).month
    return 0


def periodo(inicio, fin):
    if inicio and fin:
        return int(
            (util.convert_fecha(fin) -
             util.convert_fecha(inicio)).days)
    else:
        return 0


def vigencia(fecha, vigencia, periodo=False):
    fecha_converted = None
    if fecha:
        fecha_converted = util.convert_fecha(fecha)
    if fecha and vigencia and '-' not in vigencia:
        fecha_fin = None
        cantidad = [int(s) for s in vigencia.split() if s.isdigit()][0]
        if 'D' in vigencia:
            fecha_fin = fecha_converted + datetime.timedelta(days=cantidad)
        elif 'M' in vigencia:
            fecha_fin = fecha_converted + relativedelta(months=+cantidad)
        elif 'A' in vigencia:
            fecha_fin = fecha_converted + relativedelta(years=+cantidad)
        if fecha_fin and periodo:
            return (fecha_fin - fecha_converted).days
        if fecha_fin and not periodo:
            return fecha_fin.month
    return 0


def firma_contrato(fecha_firma_contrato, fecha_adjudicacion=None):
    fecha_firma_contrato_converted = None
    fecha_adjudicacion_converted = None
    if fecha_firma_contrato:
        fecha_firma_contrato_converted = util.convert_fecha(fecha_firma_contrato)
    if fecha_adjudicacion:
        fecha_adjudicacion_converted = util.convert_fecha(fecha_adjudicacion)
    if fecha_firma_contrato and not fecha_adjudicacion:
        return fecha_firma_contrato_converted.month
    if fecha_firma_contrato and fecha_adjudicacion:
        return (fecha_firma_contrato_converted - fecha_adjudicacion_converted).days * 86400
    return 0


def proveedor_ciudad(departamento, ciudad, pais):
    return hash_value(limpiar_nombre_ciudad(departamento, ciudad,
                                            pais), False, True)


def forma_adjudicacion(forma):
    if not forma:
        return 0
    if forma == 'L':
        return 1
    if forma == 'I':
        return 2
    if forma == 'T':
        return 3
    if forma == 'C':
        return 4
    return 0


def transform(result, etapa):
    """
    Método que transforma los datos obtenidos de la base de datos.
    :param result:
    :param etapa:
    :return:
    """

    result['marcas_sin_transformar'] = result['marcas']
    result['forma_adjudicacion_descripcion'] = result['forma_adjudicacion']
    result['marcas'] = result.apply(lambda row: convertir_marcas(row['marcas'], 'etiquettes'), axis=1)
    result['pac_monto_total'] = result.apply(lambda row: monto(row['pac_monto_total'], row['moneda'],
                                                               row['pac_fecha_publicacion']), axis=1)
    result['llamado_monto_total'] = result.apply(lambda row: monto(row['llamado_monto_total'],
                                                                   row['moneda'],
                                                                   row['llamado_fecha_publicacion']), axis=1)
    result['llamado_consultas_fin'] = result.apply(lambda row: convert_fecha(row['fecha_tope_consulta']), axis=1)

    result['fecha_llamado_menos_fecha_entrega'] = result.apply(lambda row: periodo(row['llamado_fecha_publicacion'],
                                                                                   row['fecha_entrega_oferta']), axis=1)

    result['fecha_consulta_menos_fecha_entrega'] = result.apply(lambda row: periodo(row['fecha_tope_consulta'],
                                                                                    row['fecha_entrega_oferta']),
                                                                axis=1)

    result['fecha_llamado_menos_fecha_consulta'] = result.apply(lambda row: periodo(row['llamado_fecha_publicacion'],
                                                                                    row['fecha_tope_consulta']), axis=1)

    result['fecha_entrega_oferta'] = result.apply(lambda row: convert_fecha(row['fecha_entrega_oferta']), axis=1)

    result['llamado_fecha_publicacion'] = result.apply(lambda row: convert_fecha(row['llamado_fecha_publicacion']),
                                                       axis=1)
    result['pac_fecha_publicacion'] = result.apply(lambda row: convert_fecha(row['pac_fecha_publicacion']), axis=1)
    result['llamado_tipos_documentos'] = result.apply(
        lambda row: convertir_marcas(row['tipo_documentos_llamado'],
                                     'tenderDocumentType'), axis=1)
    result['modalidad'] = result.apply(lambda row: hash_value(row.get('modalidad'), False, True),
                                       axis=1)
    result['forma_adjudicacion'] = result.apply(lambda row: forma_adjudicacion(row.get('forma_adjudicacion')), axis=1)
    if etapa == Etapas.CONTRATO.name:
        result['ajudicacion_mes_publicacion'] = result.apply(
            lambda row: convert_fecha(row['ajudicacion_fecha_publicacion']), axis=1)
        result['adjudicacion_monto_total'] = result.apply(
            lambda row: monto(row['adjudicacion_monto_total'], row['moneda'], row['ajudicacion_fecha_publicacion']),
            axis=1)
        result['contrato_mes_firma'] = result.apply(lambda row: firma_contrato(row['fecha_contrato']), axis=1)
        result['contrato_mes_inicio'] = result.apply(lambda row: firma_contrato(row['fecha_contrato']), axis=1)
        result['fecha_firma_menos_fecha_adjudicacion'] = result.apply(
            lambda row: firma_contrato(row['fecha_contrato'], row['ajudicacion_fecha_publicacion']), axis=1)

        result['contrato_mes_fin'] = result.apply(lambda row: vigencia(row['fecha_contrato'],
                                                                       row['vigencia_contrato'],
                                                                       False), axis=1)
        result['contrato_periodo'] = result.apply(
            lambda row: vigencia(row['fecha_contrato'], row['vigencia_contrato'], True),
            axis=1)
        result['monto_contrato'] = result.apply(
            lambda row: monto(row.get('proveedor_monto_adjudicado'),
                              row['moneda'],
                              row['fecha_contrato'] if row['fecha_contrato'] else row['ajudicacion_fecha_publicacion']),
            axis=1)
        result['contrato_tipo_documentos'] = result.apply(
            lambda row: convertir_marcas(row['tipo_documentos_contrato'], 'contractDocumentType'), axis=1)
        result['proveedor_identificador'] = result.apply(
            lambda row: hash_value(row.get('ruc'),
                                   row.get('proveedor'),
                                   False, True), axis=1)
        result['proveedor_ciudad'] = result.apply(
            lambda row: proveedor_ciudad(row.get('proveedor_departamento'), row.get('proveedor_ciudad'),
                                         row.get('proveedor_pais')), axis=1)

    # Reordenamos las columnas, deben estar en este orden si o si para que de los mismos resultados que
    # el modelo entrenado

    if not etapa == Etapas.CONTRATO.name:
        result = result[llamados_columnas_en_orden]
    else:
        result = result[contratos_columnas_en_orden]

    return result
