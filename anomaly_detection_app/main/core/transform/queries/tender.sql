SELECT
  (SELECT string_agg(marcas, ';') AS marcas
   FROM
     (SELECT
        nro_licitacion,
        regexp_split_to_table(marca, ',') AS marcas
      FROM indice.licitacion) AS t
   WHERE nro_licitacion = sicp.pac.nro_pac
   GROUP BY nro_licitacion),
  (SELECT array_to_string(array_agg(tipo_documento), ';') AS tipo_documentos_llamado
   FROM
     (

       SELECT
         	case  when td.id = 37 then
				'awardNotice'
			when td.id = 15 then
				 'biddingDocuments'
			when td.id = 11 then
				 'technicalSpecifications'
			when td.id = 36 then
				 'evaluationReports'
			when td.id = 228 then
				 'contractSigned'
			when td.id = 282 then
				 'contractArrangements'
			when td.id = 25 then
				 'clarifications'
			when td.id = 45 then
				 'environmentalImpact'
			when td.id = 165 then
				 'winningBid'
        ELSE
         td.descripcion END AS tipo_documento

       FROM sicp.llamado_adjunto doc
         INNER JOIN sicp.llamado llad ON llad.id = doc.llamado_id
         INNER JOIN sicp.tipo_documento td ON doc.tipo_documento_id = td.id
         join sicp.pac on llad.pac_id = pac.id
       WHERE llad.publico = TRUE
             AND ((doc.publicable IS NULL AND td.publicable IS TRUE) OR (doc.publicable IS TRUE))
             AND doc.estado = 'A'
             AND doc.correcto = TRUE
             AND doc.verificado = TRUE
             AND llad.id = lla.id

     )  tt),
  TO_CHAR(pac.fecha_publicacion,
          'yyyy-mm-ddThh24:MI:SSz') as pac_fecha_publicacion,
    TO_CHAR(lla.fecha_publicacion,
          'yyyy-mm-ddThh24:MI:SSz') as llamado_fecha_publicacion,
  CASE when pac.fecha_publicacion < '2015-07-07' THEN 0 else
  pac.monto_total end                       AS pac_monto_total,
  pac.moneda                             AS moneda,
  lla.slug                               AS slug,
  TO_CHAR(lla.fecha_entrega_oferta,
          'yyyy-mm-ddThh24:MI:SSz') AS fecha_entrega_oferta,
  case when lla.fecha_publicacion < '2015-07-07' THEN 0 else
  lla.monto_total  end                      AS llamado_monto_total,
  TO_CHAR(lla.fecha_tope_consulta,
          'yyyy-mm-ddThh24:MI:SSz') AS fecha_tope_consulta,
    lla.forma_adjudicacion as forma_adjudicacion,
         modalidad.codigo as modalidad,
       categoria.codigo as categoria,
       licitacion.entidad_codigo_sicp as entidad,
  modalidad.codigo as modalidad_descripcion,
      categoria.descripcion as categoria_descripcion,
       licitacion.nombre_convocante as entidad_descripcion

FROM sicp.llamado lla
    JOIN sicp.modalidad on lla.modalidad_id = modalidad.id
    join indice.licitacion on lla.id = licitacion.llamado_id
  JOIN sicp.pac ON lla.pac_id = pac.id
                   AND lla.publico = TRUE
                   AND pac.publico = TRUE
                   AND pac.anio >= 2010
                   AND lla.decreto_5174 = FALSE
                   AND lla.estado IN
                       ('PUB', 'IMP_SUS', 'IMP_PAR', 'IM_PAR_SUS', 'CIMP', 'CAN', 'DES', 'SUS', 'ADJ', 'ANU', 'ANU_PAR')
            JOIN sicp.categoria on pac.categoria_id = categoria.id

WHERE {0}
ORDER BY pac.anio desc