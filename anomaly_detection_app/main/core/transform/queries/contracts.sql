SELECT
  (SELECT string_agg(marcas, ';') AS marcas
   FROM
     (SELECT
        nro_licitacion,
        regexp_split_to_table(marca, ',') AS marcas
      FROM indice.licitacion) AS t
   WHERE nro_licitacion = sicp.pac.nro_pac
   GROUP BY nro_licitacion),
  (SELECT array_to_string(array_agg(tipo_documento), ';') AS tipo_documentos_llamado
   FROM
     (

       SELECT
         	case  when td.id = 37 then
				'awardNotice'
			when td.id = 15 then
				 'biddingDocuments'
			when td.id = 11 then
				 'technicalSpecifications'
			when td.id = 36 then
				 'evaluationReports'
			when td.id = 228 then
				 'contractSigned'
			when td.id = 282 then
				 'contractArrangements'
			when td.id = 25 then
				 'clarifications'
			when td.id = 45 then
				 'environmentalImpact'
			when td.id = 165 then
				 'winningBid'
        ELSE
         td.descripcion END AS tipo_documento

       FROM sicp.llamado_adjunto doc
         INNER JOIN sicp.llamado llad ON llad.id = doc.llamado_id
         INNER JOIN sicp.tipo_documento td ON doc.tipo_documento_id = td.id
         join sicp.pac on llad.pac_id = pac.id
       WHERE llad.publico = TRUE
             AND ((doc.publicable IS NULL AND td.publicable IS TRUE) OR (doc.publicable IS TRUE))
             AND doc.estado = 'A'
             AND doc.correcto = TRUE
             AND doc.verificado = TRUE
             AND llad.id = lla.id

     )  tt),
  TO_CHAR(pac.fecha_publicacion,
          'yyyy-mm-ddThh24:MI:SSz') as pac_fecha_publicacion,
    TO_CHAR(lla.fecha_publicacion,
          'yyyy-mm-ddThh24:MI:SSz') as llamado_fecha_publicacion,
  CASE when pac.fecha_publicacion < '2015-07-07' THEN 0 else
  pac.monto_total end                       AS pac_monto_total,
  pac.moneda                             AS moneda,
  lla.slug                               AS llamado_slug,
  TO_CHAR(lla.fecha_entrega_oferta,
          'yyyy-mm-ddThh24:MI:SSz') AS fecha_entrega_oferta,
  case when lla.fecha_publicacion < '2015-07-07' THEN 0 else
  lla.monto_total  end                      AS llamado_monto_total,
  TO_CHAR(lla.fecha_tope_consulta,
          'yyyy-mm-ddThh24:MI:SSz') AS fecha_tope_consulta,
    lla.forma_adjudicacion as forma_adjudicacion,
         modalidad.codigo as modalidad,
       categoria.codigo as categoria,
       licitacion.uc_codigo_sicp as unidad_contratacion,
              licitacion.entidad_codigo_sicp as entidad,

       -- contrato y adjudicacion


  adj.slug as slug,
  to_char(adj.fecha_publicacion,'yyyy-mm-ddThh24:MI:SSz') as ajudicacion_fecha_publicacion,
  adj.monto_total as adjudicacion_monto_total,
  pa.slug as proveedor_adjudicado_slug,

      (
        CASE
          WHEN pa.duracion_contrato_medida IS NOT NULL
            AND pa.duracion_contrato_cantidad IS NOT NULL
          THEN sicp.formateo_duracion(pa.duracion_contrato_cantidad,pa.duracion_contrato_medida)
          WHEN pa.tipo_vigencia_contrato = 'HCUMP'
          THEN
          null
          WHEN  pa.tipo_vigencia_contrato = 'HREC'
          THEN
          NULL
          WHEN  pa.tipo_vigencia_contrato = 'FEC'
          THEN TEXT(pa.duracion_contrato_fecha_hasta)
        ELSE
          (
            COALESCE
            (
             (
              SELECT
                CASE
                  WHEN cont.marca_fecha_vigencia_desde=TRUE THEN TEXT(pa.fecha_contrato)
                  ELSE TEXT(cont.fecha_vigencia_desde)
                END
                FROM sigc.contrato cont INNER JOIN sicp.proveedor_adjudicado pa1 ON cont.proveedor_adjudicado_id = pa1.id
                WHERE pa1.proveedor_adjudicado_publico_id = pa.id AND pac.nro_pac NOT IN (SELECT nro_pac FROM sicp.pacs_sin_tope)
                AND cont.estado_contrato_id = 5
              ),
              TEXT(pa.vigencia_desde)
            )
            || '-' ||
            COALESCE
            (
             (
              SELECT
               CASE
                WHEN cont.vigencia_hasta=TRUE THEN (cont.nro_vigencia_hasta ||' '|| cont.descripcion_vigencia_hasta)
                ELSE TEXT(cont.fecha_vigencia_hasta)
               END
               FROM sigc.contrato cont INNER JOIN sicp.proveedor_adjudicado pa1 ON cont.proveedor_adjudicado_id = pa1.id
               WHERE pa1.proveedor_adjudicado_publico_id = pa.id AND pac.nro_pac NOT IN (SELECT nro_pac FROM sicp.pacs_sin_tope)
               AND cont.estado_contrato_id = 5
             ),
             TEXT(pa.vigencia_hasta)
            )
          )
      END) AS vigencia_contrato,
TO_CHAR(pa.fecha_contrato, 'yyyy-mm-ddThh24:MI:SSz') as fecha_contrato,
  CASE when p_estado.codigo = 'ADJ' THEN
    'active'
  when p_estado.codigo = 'CAN' then
    'cancelled'
  else
    'pending' END as estado_contrato,
      (SELECT array_to_string(array_agg(tipo_documento), ';') AS tipo_documentos_contrato
        from
          (
            SELECT
         	case  when id = 37 then
				'awardNotice'
			when id = 15 then
				 'biddingDocuments'
			when id = 11 then
				 'technicalSpecifications'
			when id = 36 then
				 'evaluationReports'
			when id = 228 then
				 'contractSigned'
			when id = 282 then
				 'contractArrangements'
			when id = 25 then
				 'clarifications'
			when id = 45 then
				 'environmentalImpact'
			when id = 165 then
				 'winningBid'
        ELSE
         tipo_documento END AS tipo_documento from (
               (select
                 td.id,
      td.descripcion AS tipo_documento
    FROM sicp.adjudicacion_adjunto_proveedor doc
    INNER JOIN sicp.tipo_documento td ON doc.tipo_documento_id = td.id
    WHERE
        doc.estado = 'PUB'
      and doc.proveedor_adjudicado_id = pa.id
      AND doc.activo = TRUE
      AND doc.publicar = TRUE
      AND ( (doc.publicable IS NULL AND td.publicable IS TRUE) OR (doc.publicable IS TRUE) )
      AND doc.verificado = TRUE

    )
    UNION
    (
    SELECT
                 td.id,
     td.descripcion AS tipo_documento
    FROM sicp.adjudicacion_adjunto_entidad doc
    INNER JOIN sicp.adjudicacion adj ON adj.id = doc.adjudicacion_id       AND adj.publico = TRUE and adj.llamado_id = lla.id
    INNER JOIN sicp.tipo_documento td ON doc.tipo_documento_id = td.id       AND td.interno = FALSE
    WHERE

( (td.publicable is true) or (doc.publicable is true) )

      AND doc.estado = 'A'
      AND doc.publicar = TRUE
      AND doc.verificado = TRUE

    )
    UNION
    (
    SELECT
                 td.id,
     td.descripcion tipo_documento
    FROM sicp.rescision_contrato_documento_adjunto doc
    INNER JOIN sicp.rescision_contrato resc ON doc.rescision_contrato_id = resc.id AND resc.estado_rescision = 'PUB'
    INNER JOIN sicp.tipo_documento td ON td.id = doc.tipo_documento_id
    WHERE doc.correcto = TRUE and resc.proveedor_adjudicado_id = pa.id
      AND doc.verificado = TRUE

    )) t)tt),
        COALESCE(pa.razon_social_proveedor_adjudicado, p.razon_social) AS proveedor,
      COALESCE(p.ruc_completo, p.ruc) AS ruc,
  p.email as proveedor_email,
  p.direccion as proveedor_direccion,
 p.ciudad as proveedor_ciudad,
p_departamento.descripcion as               proveedor_departamento,
p_pais.descripcion as proveedor_pais,
  coalesce(pa.monto_adjudicado , 0) as proveedor_monto_adjudicado,
         modalidad.codigo as modalidad_descripcion,
      categoria.descripcion as categoria_descripcion,
  count(o.id) as numero_oferentes
FROM sicp.llamado lla
        JOIN sicp.modalidad on lla.modalidad_id = modalidad.id
    join indice.licitacion on lla.id = licitacion.llamado_id

  JOIN sicp.pac ON lla.pac_id = pac.id
                   AND lla.publico = TRUE
                   AND pac.publico = TRUE
                   AND lla.decreto_5174 = FALSE
                   AND lla.estado IN
                       ('PUB', 'IMP_SUS', 'IMP_PAR', 'IM_PAR_SUS', 'CIMP', 'CAN', 'DES', 'SUS', 'ADJ', 'ANU', 'ANU_PAR')
 JOIN sicp.categoria on pac.categoria_id = categoria.id
  join sicp.adjudicacion adj on adj.llamado_id = lla.id and adj.publico is true
  LEFT JOIN sicp.proveedor_oferente o ON o.adjudicacion_id = adj.id
  JOIN sicp.proveedor_adjudicado pa on adj.id = pa.adjudicacion_id AND pa.estado in ('ADJ','CAN','ANU','SUS') and
  pa.proveedor_adjudicado_publico_id is null
      INNER JOIN sicp.parametro p_estado ON (p_estado.dominio = '_ESTADO_CONTRATO'
      AND p_estado.codigo =  sicp.estado_contrato(pa.estado))
      INNER JOIN sipe.proveedor p ON pa.proveedor_id = p.id
        LEFT JOIN sicp.parametro p_departamento ON p.departamento = p_departamento.codigo AND p_departamento.dominio = 'DEPARTAMENTO_PARAGUAY'
      LEFT JOIN sicp.parametro p_pais ON p.pais = p_pais.codigo AND p_pais.dominio = 'PAIS'
WHERE {}
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28, 29, 30, 31, 32, 33
order by pac_fecha_publicacion;