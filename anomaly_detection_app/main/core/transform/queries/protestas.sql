--﻿------ DENUNCIAS -------
/* Este listado devuelve solo las denuncias en las cuales se ha realizado al menos sobre un ID de licitación. Existen denuncias realizadas en las cuales no se especifico en el proceso el ID pero se hace mención en eventos posteriores o documentos adjuntos*/
/* Este listado no verifica el estado actual de los procedimientos de contratación*/
---## PROCEDIMIENTO DE CONTRATACION ##---

SELECT * FROM (
select ll.slug as llamado_slug, null as proveedor_adjudicado_slug
 --ll.*
 from sicp.llamado ll
                join sicp.pac p on ll.pac_id = p.id
where ll.publico = true and p.publico=true
                AND nro_pac IN (
select llt.nro_pac from stje.tramite as tramite
join stje.llamado_tramite as llt on llt.tramite_id=tramite.id
where tramite.tipo_tramite_id=4
and tramite.tramite_acumulado_id is null
and tramite.fecha_escrito between '{0}-01-01' and '{1}-12-31 23:59:59'

and tramite.activo=true order by tramite.fecha_escrito desc)

UNION (

------- CONSULTA PARA PROTESTAS CON RESULTADO A FAVOR DEL PROTESTANTE ---------
---## PROCESOS CON CONTRATO ##---
/* Se acota que para estos casos no se puede establcer sobre cual proveedor se realizó la protesta ya que el mismo se realiza sobre el ID de la licitación */
select distinct ll.slug as llamado_slug, pa.slug as proveedor_adjudicado_slug
       --pa.*
       from sicp.llamado ll
       join sicp.adjudicacion a on a.llamado_id = ll.id
       left join sicp.proveedor_adjudicado pa on pa.adjudicacion_id = a.id
        join sicp.pac p on ll.pac_id = p.id
where
                pa.codigo_contratacion_proveedor_adjudicado is not null
                and pa.proveedor_adjudicado_publico_id is null
                AND p.publico=true
                AND nro_pac IN (
                               select tramite.nro_pac from stje.atributo as atributo
                                               join stje.evento as evento on evento.id=atributo.evento_id
                                               join stje.tramite as tramite on tramite.id=evento.tramite_id
                               where atributo.atributo_evento_id=64                              ---- atributo tipo de cierre
                                               and atributo.cadena_corta in ('C_LUG_TOT','C_LUG_PARC')  ---- Hacer lugar Parcialmente, Hacer Lugar Totalmente
                                               and tramite.tipo_tramite_id in (1,2,3)   ---- tipos de tramite modulo protestas
                                               and evento.erroneo=false
                                               and tramite.activo=true
                                               and atributo.booleano=true
                                               and tramite.fecha_escrito between '{0}-01-01' and '{1}-12-31 23:59:59'
                               order by tramite.fecha_escrito desc
) )) as protestas_denuncias
--en la columna pa.estado_proveedor_adjudicado existen valor de: "IC", "PUBCSL"

