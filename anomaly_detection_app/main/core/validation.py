import datetime
import os

import pandas as pd
from joblib import load

from anomaly_detection_app.main import util
from anomaly_detection_app.main.config import variables_categoricas, TRAINING_DATA_PATH
from anomaly_detection_app.main.core.transform.queries import QUERIES_PATH
from anomaly_detection_app.main.core.transform.transform import get_and_transform_bulk
from anomaly_detection_app.main.core.util import preparar_datos_entrenamiento, guardar_predicciones
from anomaly_detection_app.main.models.entrenamiento import Entrenamiento, Etapas

PROTESTAS_QUERY_FILE = os.path.join(QUERIES_PATH, 'protestas.sql')


def validar(modelo_id, anio_inicio, anio_fin, etapa):
    connection = None
    try:
        connection = util.get_connection()
        csv_file = get_and_transform_bulk(anio_inicio, anio_fin, etapa)
        datos, input_data = preparar_datos_entrenamiento(csv_file)
        datos = datos.drop(variables_categoricas, axis=1)
        modelo = Entrenamiento.get_by_id(int(modelo_id))
        if modelo is None:
            return None
        clf = load(modelo.modelo_clasificacion_path)
        archivo, scores = guardar_predicciones(clf, datos, input_data,
                                               os.path.join(TRAINING_DATA_PATH, 'validacion-{}'.
                                                            format(datetime.datetime.today().
                                                                   strftime("%Y-%m-%d-%H:%M:%S"))))
        validaciones = pd.read_csv(archivo, sep=',')
        with open(PROTESTAS_QUERY_FILE, 'r') as query:
            protestas = pd.read_sql_query(query.read().format(anio_fin, anio_fin), connection)

        resultado = calcular_valores_validacion(protestas, validaciones, etapa)
        resultado['validation_file'] = archivo

        return resultado
    finally:
        if connection:
            connection.close()


def calcular_valores_validacion(protestas, validaciones, etapa):
    if etapa == Etapas.CONTRATO.name:
        validaciones['proveedor_adjudicado_slug'] = validaciones['slug']
    else:
        validaciones['llamado_slug'] = validaciones['slug']
    protestas['protestado'] = True
    result = pd.merge(validaciones, protestas, how='outer')
    result = result.dropna(subset=['predicted_class']).fillna(value=False)
    total_protestados = (result.loc[result['protestado'] == True].count()['protestado'])
    total_correctos = (result.loc[(result['predicted_class'] < 0) &
                                  (result['protestado'] == True)].count()['predicted_class'])
    total = (result.count()['slug'])
    total_anomalos = (result.loc[(result['predicted_class'] < 0)].count()['predicted_class'])
    return {
        'accuracy': '{}%'.format(int((total_correctos/total_protestados)*100)),
        'number_of_processes': int(total),
        'number_of_protested': int(total_protestados),
        'number_of_detected_anomalies': int(total_anomalos),
        'percentage_detected_anomalies': '{}%'.format(int((total_anomalos/total)*100))
    }
