import datetime
import logging
from urllib.parse import urlparse

import psycopg2

from anomaly_detection_app.main import config


def get_connection():
    """ Método que toma la configuración del archivo config.py para
    crear una conexión a la base de datos

    Returns:
        connection: La conexión a la base de datos
    """
    try:
        result = urlparse(config.database)
        username = result.username
        password = result.password
        database = result.path[1:]
        hostname = result.hostname
        connection = psycopg2.connect(
            database=database,
            user=username,
            password=password,
            host=hostname
        )
        return connection
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)


def execute_query(select_query, params, get=False, insert=False):
    conn = None
    cursor = None
    try:
        conn = get_connection()
        cursor = conn.cursor()
        if insert:
            select_query += ' RETURNING id'
        if params:
            cursor.execute(select_query, params)
        else:
            cursor.execute(select_query)
        if get:
            return cursor.fetchone()
        if insert:
            return cursor.fetchone()[0]
        else:
            return None
    except Exception as e:
        logging.error('Error al ejecutar {} - {}'.format(select_query, e))
        return None
    finally:
        if cursor:
            cursor.close()
        if conn:
            conn.commit()
            conn.close()


def convert_fecha(fecha):
    return datetime.datetime.strptime(fecha, '%Y-%m-%dT%H:%M:%SZ')

