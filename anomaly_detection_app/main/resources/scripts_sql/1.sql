CREATE TABLE sicp.cotizacion (
  id  bigserial   not null
    constraint cotizacion_pkey
    primary key,
  fecha timestamp(0) not null,
  compra numeric not null,
  venta numeric not null
);

CREATE USER anomalias WITH PASSWORD 'anomalias';
ALTER ROLE "anomalias" WITH LOGIN;
CREATE SCHEMA anomalias;
ALTER SCHEMA anomalias OWNER TO anomalias;
GRANT ALL ON SCHEMA anomalias TO anomalias;

GRANT USAGE ON SCHEMA sicp, sipe, sbe, sigc, indice, stje TO anomalias;

GRANT SELECT ON TABLE sicp.llamado, sicp.pac, sbe.subasta, sicp.proveedor_adjudicado, sicp.adjudicacion,
    sicp.proveedor_oferente, sicp.parametro, sipe.proveedor, stje.tramite, stje.atributo, stje.atributo_evento,
    stje.llamado_tramite, stje.evento, sicp.categoria, sicp.modalidad TO anomalias;

GRANT SELECT ON TABLE indice.licitacion, sicp.llamado_adjunto, sicp.adjudicacion_adjunto_proveedor,
    sicp.adjudicacion_adjunto_entidad, sicp.rescision_contrato_documento_adjunto, sicp.rescision_contrato, sicp.parametro,
    sicp.tipo_documento, sigc.contrato, sicp.pacs_sin_tope TO anomalias;

GRANT SELECT, INSERT ON TABLE sicp.cotizacion TO anomalias;

GRANT SELECT, USAGE ON SEQUENCE sicp.cotizacion_id_seq TO anomalias;

CREATE TABLE anomalias.entrenamiento (
    id  bigserial   not null
    constraint anomalias_entrenamiento_pkey
    primary key,
    fecha_inicio timestamp,
    fecha_fin timestamp,
    modelo_clasificacion_path text,
    modelo_interpretacion_path text,
    log_path text,
    datos_path text,
    activo boolean DEFAULT FALSE,
    estado varchar(10) DEFAULT 'ENTRENANDO',--ENTRENANDO, ERROR, ENTRENADO,
    etapa varchar(12) DEFAULT 'CONVOCATORIA'--CONVOCATORIA, CONTRATO
);

ALTER TABLE anomalias.entrenamiento OWNER TO anomalias;
GRANT SELECT, USAGE ON SEQUENCE anomalias.entrenamiento_id_seq TO anomalias;