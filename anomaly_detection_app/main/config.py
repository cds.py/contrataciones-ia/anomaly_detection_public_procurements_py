import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
ROOT_PATH = os.path.abspath(os.path.join(BASE_DIR, os.pardir, os.pardir))


class Config:
    SQLALCHEMY_DATABASE_URI = os.getenv('DB_URL')
    TRAINING_DATA_PATH = os.getenv('TRAINING_DATA_PATH', os.path.join(ROOT_PATH, 'datos'))
    MODELS_DATA_PATH = os.getenv('MODELS_DATA_PATH', os.path.join(ROOT_PATH, 'modelos'))
    LOG_PATH = os.getenv('LOG_PATH', os.path.join(ROOT_PATH, 'logs'))


config_by_name = dict(
    dev=Config
)

database = Config.SQLALCHEMY_DATABASE_URI
TRAINING_DATA_PATH = Config.TRAINING_DATA_PATH
MODELS_DATA_PATH = Config.MODELS_DATA_PATH
LOG_PATH = Config.LOG_PATH

for paths in [TRAINING_DATA_PATH, MODELS_DATA_PATH, LOG_PATH]:
    if not os.path.exists(paths):
        os.mkdir(paths)

decision_tree_drop_columns = ['slug', 'llamado_tipos_documentos', 'contrato_tipo_documentos',
                              'proveedor_ciudad', 'proveedor_identificador',
                              'marcas_sin_transformar', 'modalidad', 'categoria', 'entidad', 'forma_adjudicacion']

BCP_PAGE_URL = 'https://www.bcp.gov.py/webapps/web/cotizacion/referencial-fluctuante'

# EN SEGUNDOS, CADA 23 HORAS
COTIZACIONES_SCHEDULER_TIME = 60 * 60 * 23

MODELS_FOLDER_NAME = 'core/trained_models'

QUARTIL_1 = -0.18

marcas = ['plurianual', 'contrato_abierto', 'fonacide', 'subasta', 'adreferendum', 'produccion_nacional', 'impugnado',
          'licitacion_sin_difusion', 'abastecimiento_simultaneo', 'urgencia_impostergable', 'precalificacion',
          'agricultura_familiar', 'bs_estrategicos', 'convenio_marco', 'seguridad_nacional']

categorias = ["Servicios Técnicos",
              "Servicios de Limpiezas, Mantenimientos y reparaciones menores y mayores de Instalaciones, Maquinarias y Vehículos",
              "Materiales e insumos eléctricos, metálicos y no metálicos, Plásticos, cauchos. Repuestos, herramientas, cámaras y cubiertas.",
              "Utiles de oficina, Productos de papel y cartón e impresos",
              "Equipos Militares y de Seguridad. Servicio de Seguridad y Vigilancia",
              "Muebles y Enseres",
              "Equipos, Productos e instrumentales Médicos y de Laboratorio. Servicios asistenciales de salud",
              "Servicios de ceremonial, gastronomico y funerarios",
              "Seguros",
              "Minerales",
              "Consultorías, Asesorías e Investigaciones. Estudios y Proyectos de inversión",
              "Adquisición y Locación de inmuebles. Alquiler de muebles",
              "Utensilios de cocina y comedor, Productos de Porcelana, vidrio y loza",
              "Elementos e insumos de limpieza",
              "Textiles, vestuarios y calzados",
              "Equipos, accesorios y programas computacionales, de oficina, educativos, de imprenta, de comunicación y señalamiento",
              "Productos quimicos",
              "Productos Alimenticios",
              "Capacitaciones y Adiestramientos",
              "Maquinarias, Equipos y herramientas mayores - Equipos de transporte",
              "Pasajes y Transportes",
              "Construcción, Restauración, Reconstrucción o Remodelación y Reparación de Inmuebles",
              "Publicidad y Propaganda",
              "Bienes e insumos agropecuario y forestal",
              "Combustibles y Lubricantes"]

formas_adjudicacion = ['L', 'T', 'C', 'I']

modalidades = ['LCO', 'AI', 'LC', 'CD', 'SBPF',
               'CE',
               'LPI',
               'SBCC',
               'CO',
               'SBC',
               'AN',
               '3CV',
               'SCC',
               'LPN',
               'SSF',
               'SBMC']

variables_categoricas = ['marcas_sin_transformar', 'modalidad_descripcion',
                         'categoria_descripcion']

llamados_columnas_en_orden = ['slug',
                              'marcas',
                              'llamado_monto_total',
                              'llamado_fecha_publicacion',
                              'pac_fecha_publicacion',
                              'fecha_llamado_menos_fecha_entrega',
                              'fecha_consulta_menos_fecha_entrega',
                              'fecha_llamado_menos_fecha_consulta',
                              'fecha_entrega_oferta',
                              'llamado_consultas_fin',
                              'llamado_tipos_documentos',
                              'forma_adjudicacion',
                              'modalidad',
                              'categoria',
                              'entidad',
                              'marcas_sin_transformar',
                              'modalidad_descripcion',
                              'categoria_descripcion']

contratos_columnas_en_orden = ['slug',
                               'marcas',
                               'llamado_monto_total',
                               'llamado_fecha_publicacion',
                               'pac_fecha_publicacion',
                               'fecha_llamado_menos_fecha_entrega',
                               'fecha_consulta_menos_fecha_entrega',
                               'fecha_llamado_menos_fecha_consulta',
                               'fecha_entrega_oferta',
                               'llamado_consultas_fin',
                               'llamado_tipos_documentos',
                               'forma_adjudicacion',
                               'modalidad',
                               'categoria',
                               'entidad',
                               'numero_oferentes',
                               'ajudicacion_mes_publicacion',
                               'adjudicacion_monto_total',
                               'contrato_mes_inicio',
                               'contrato_mes_fin',
                               'contrato_periodo',
                               'fecha_firma_menos_fecha_adjudicacion',
                               'contrato_mes_firma',
                               'monto_contrato',
                               'contrato_tipo_documentos',
                               'proveedor_identificador',
                               'proveedor_ciudad',
                               'marcas_sin_transformar',
                               'modalidad_descripcion',
                               'categoria_descripcion']
