from datetime import datetime

from flask import request
from flask_restplus import Resource, Namespace, fields

from anomaly_detection_app.main.core.training import entrenar
from anomaly_detection_app.main.core.validation import validar
from anomaly_detection_app.main.models.entrenamiento import Entrenamiento, Etapas, EntrenamientoEstado, row2dict
from anomaly_detection_app.main.services.models_service import predecir

api = Namespace('modelos', description='Modelos de entrenamiento de detección de anomalías')

model = api.model('Predecir Model',
                  {'convocatorias_slug': fields.List(fields.String, description='Slugs de las convocatorias ',
                                                     required=True)})

contrato_model = api.model('Predecir Contrato Model', {'slugs': fields.List(fields.String,
                                                                            description='Slugs de los contratos',
                                                                            required=True)})


@api.route('/<etapa>')
class Modelos(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Internal Server Error', 404: 'Not found'},
             params={'etapa': '{} o {}'.format(Etapas.CONTRATO.name, Etapas.CONVOCATORIA.name),
                     'estado': '{}, {} o {}'.format(EntrenamientoEstado.ENTRENADO.name,
                                                    EntrenamientoEstado.ENTRENANDO.name,
                                                    EntrenamientoEstado.ERROR.name),
                     'entrenamiento_id': 'Id del modelo a buscar',
                     'activo': 'Búsqueda por entrenamientos activos o inactivos'})
    def get(self, etapa):
        """
        Retorna la lista de modelos entrenados existentes. Por defecto retrona solamente los entrenamientos activos
        y con estado ENTRENADO
        """
        estado = request.args.get('estado', None)
        activo = request.args.get('activo', True)
        entrenamiento_id = request.args.get('entrenamiento_id', None)
        if entrenamiento_id:
            entrenamientos = row2dict(Entrenamiento.get_by_id(entrenamiento_id))
        else:
            entrenamientos = Entrenamiento.get_by_etapa_tipo_estado(etapa, estado, activo)
        if not entrenamientos:
            return {'mensaje': 'No encontrado'}, 404
        return entrenamientos


@api.route('/activar/<id>')
class ModelosActivar(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Internal Server Error', 404: 'Not found'},
             params={'id': 'Id del modelo a activar'})
    def put(self, id):
        """
        Activa un modelo de predicción dado. Activar un modelo hará que los demás de la misma etapa y tipo se desactiven
        ya que solo un entrenamiento por etapa y tipo puede estar activo al mismo tiempo.
        """
        entrenamiento = Entrenamiento.get_by_id(id)
        if not entrenamiento:
            return {'mensaje': 'No encontrado'}, 404
        entrenamiento.activar()
        return {'mensaje': 'Activado correctamente'}


@api.route('/predecir/convocatoria')
class PrediccionConvocatoria(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Internal Server Error', 404: 'Not found'})
    @api.expect(model)
    def post(self):
        """
        Realiza la clasificación del proceso para determinar si es anómalo o no. Recibe como parámetros la lista de
        slugs de las convocatorias a clasificar.

        Valores menores a 0 indican que es un proceso anómalo y mayores a 0 que es un proceso normal. Para
        predecir se utiliza el modelo activo para el tipo y etapa dadas. Si ningun modelo está activo
        se utiliza en original.
        """

        return predecir(request.json)


@api.route('/predecir/contrato')
class PrediccionContrato(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Internal Server Error', 404: 'Not found'})
    @api.expect(contrato_model)
    def post(self):
        """
        Realiza la clasificación del contrato para determinar si es anómalo o no. Recibe como parámetro los slugs
        de los contratos a clasificar.

        Valores menores a 0 indican que es un contrato anómalo y mayores a 0 que es un contrato normal. Se utiliza para
        predecir el modelo activo para el tipo y etapa dadas. Si ningun modelo está activo se utiliza en original.
        """

        return predecir(request.json)


@api.route('/entrenar/<etapa>')
class Entrenar(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Internal Server Error', 404: 'Not found'},
             params={'inicio': 'Año inicio. Se utiliza el año del PAC para la extracción de los datos',
                     'fin': 'Año de fin. Se utiliza el año del PAC para la extracción de los datos',
                     'etapa': '{} o {}'.format(Etapas.CONTRATO.name, Etapas.CONVOCATORIA.name)
                     })
    def post(self, etapa):
        """
        Entrena un nuevo modelo de predicción según los parámetros pasados.

        El servicio entrena el modelo en background. Una vez que el modelo esté completo se creará un joblib nuevo en
        la carpeta de modelos configurada. Además será listado en el servicio de lista de modelos existentes.
        El estado del entrenamiento puede ser consultado en la tabla de entrenamientos o mediante el servicio de modelos
        pasando el id del modelo retornado por este servicio
        """
        try:
            inicio = int(request.args.get('inicio', 2010))
            fin = int(request.args.get('fin', datetime.today().year))
            if inicio > fin:
                return {'mensaje': 'El año de inicio debe ser menor al de fin'}, 400
            entrenamiento = Entrenamiento(fecha_inicio=datetime.today(), etapa=etapa,
                                          estado=EntrenamientoEstado.ENTRENANDO.name, activo=False)
            entrenamiento.save()
            entrenar(entrenamiento, inicio, fin, etapa)
        except ValueError:
            return {'mensaje': 'El año de inicio y fin deben ser numéricos'}, 400

        return {'mensaje': 'Entrenando', 'entrenamiento_id': entrenamiento.id}


@api.route('/validar/<etapa>/<modelo_id>')
class Validar(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Internal Server Error', 404: 'Not found'},
             params={'inicio': 'Año inicio. Se utiliza el año del PAC para la extracción de los datos',
                     'fin': 'Año de fin. Se utiliza el año del PAC para la extracción de los datos',
                     'etapa': '{} o {}'.format(Etapas.CONTRATO.name, Etapas.CONVOCATORIA.name),
                     'modelo_id': 'id del modelo a validar'
                     })
    def post(self, etapa, modelo_id):
        """
        Valida un modelo de predicción según los parámetros pasados.

        Retorna:
        - El path al archivo de validación
        - El accuracy obtenido comparando con las protestas a favor y denuncias realizadas (entre más alto mejor)
        - La cantidad de protestas y denuncias en total
        - El porcentaje de anomalías encontradas (entre más bajo mejor, normalmente hasta 40%)
        """
        try:
            inicio = int(request.args.get('inicio', 2010))
            fin = int(request.args.get('fin', datetime.today().year))
            if modelo_id is None:
                return {'mensaje': 'Debe proveer un id de modelo a validar'}, 400
            if inicio > fin:
                return {'mensaje': 'El año de inicio debe ser menor al de fin'}, 400
            archivo = validar(modelo_id, inicio, fin, etapa)
            if archivo is None:
                return {'mensaje': 'El id del modelo no existe'}, 400
        except ValueError:
            return {'mensaje': 'El año de inicio y fin deben ser numéricos'}, 400

        return archivo
