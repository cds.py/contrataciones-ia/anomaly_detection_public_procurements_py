import enum
import os

from sqlalchemy.orm.exc import NoResultFound

from anomaly_detection_app.main import db
from anomaly_detection_app.main.config import MODELS_FOLDER_NAME, BASE_DIR
from anomaly_detection_app.main.util import execute_query


class EntrenamientoEstado(enum.Enum):
    ENTRENANDO = "ENTRENANDO"
    ERROR = "ERROR"
    ENTRENADO = "ENTRENADO"


class Etapas(enum.Enum):
    CONTRATO = "CONTRATO"
    CONVOCATORIA = "CONVOCATORIA"


def row2dict(row):
    d = {}
    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))

    return d


class Entrenamiento(db.Model):
    __tablename__ = 'entrenamiento'
    schema = 'anomalias'
    __table_args__ = {'schema': 'anomalias'}
    id = db.Column(db.BigInteger, primary_key=True)
    fecha_inicio = db.Column(db.DateTime)
    fecha_fin = db.Column(db.DateTime)
    modelo_clasificacion_path = db.Column(db.Text)
    modelo_interpretacion_path = db.Column(db.Text)
    log_path = db.Column(db.Text)
    datos_path = db.Column(db.Text)
    estado = db.Column(db.Text)
    etapa = db.Column(db.Text)
    activo = db.Column(db.Boolean, default=False)

    @staticmethod
    def get_all():
        return Entrenamiento.query.all()

    @staticmethod
    def get_by_etapa_tipo_estado(etapa, estado=None, activo=True):
        if estado:
            entrenamientos = Entrenamiento.query.filter_by(etapa=etapa, estado=estado, activo=activo).all()
        else:
            entrenamientos = Entrenamiento.query.filter_by(etapa=etapa, activo=activo).all()
        to_ret = []
        for entrenamiento in entrenamientos:
            to_ret.append(row2dict(entrenamiento))
        return to_ret

    @staticmethod
    def get_activo_entrenado_etapa(etapa, interpretacion=False):
        try:
            if interpretacion:
                return Entrenamiento.query.filter_by(etapa=etapa,
                                                     estado=EntrenamientoEstado.ENTRENADO.name,
                                                     activo=True).one().modelo_interpretacion_path
            else:
                return Entrenamiento.query.filter_by(etapa=etapa,
                                                     estado=EntrenamientoEstado.ENTRENADO.name,
                                                     activo=True).one().modelo_clasificacion_path
        except NoResultFound:
            return os.path.join(BASE_DIR, MODELS_FOLDER_NAME,
                                'modelo-clasificacion-{}.joblib'.format(etapa.lower()) if not interpretacion else
                                'modelo-interpretacion-{}.joblib'.format(etapa.lower()))

    def activar(self):
        entrenamientos = Entrenamiento.query.filter_by(etapa=self.etapa,
                                                       estado=EntrenamientoEstado.ENTRENADO.name, activo=True).all()
        # desactivamos los activos existentes
        for entrenamiento in entrenamientos:
            entrenamiento.activo = False
            entrenamiento.save()
        self.activo = True
        self.save()

    @staticmethod
    def get_by_id(id):
        try:
            return Entrenamiento.query.filter_by(id=id).one()
        except NoResultFound:
            return None

    def save(self):
        if not self.id:
            self.id = execute_query('INSERT INTO anomalias.entrenamiento (fecha_inicio, fecha_fin, '
                                    'modelo_clasificacion_path, modelo_interpretacion_path, log_path, '
                                    'datos_path, estado,'
                                    'etapa) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)',
                                    (self.fecha_inicio, self.fecha_fin,
                                     self.modelo_clasificacion_path, self.modelo_interpretacion_path,
                                     self.log_path, self.datos_path, self.estado,
                                     self.etapa), insert=True)
        else:
            execute_query('UPDATE anomalias.entrenamiento SET fecha_inicio = %s, fecha_fin = %s, '
                          'modelo_clasificacion_path = %s, modelo_interpretacion_path = %s, '
                          'log_path = %s, datos_path = %s , estado = %s , etapa = %s,'
                          'activo = %s where id = %s',
                          (self.fecha_inicio, self.fecha_fin,
                           self.modelo_clasificacion_path, self.modelo_interpretacion_path,
                           self.log_path, self.datos_path, self.estado,
                           self.etapa, self.activo, self.id))
