from anomaly_detection_app.main.core.prediction import predict_convocatoria, predict_contrato


def predecir(ids):
    a_ret = []
    if 'convocatorias_slug' in ids:
        for convocatoria_slug in ids['convocatorias_slug']:
            prediccion = predict_convocatoria(convocatoria_slug)
            if prediccion is not None:
                a_ret.append(prediccion)
    else:
        for slug in ids['slugs']:
            prediccion = predict_contrato(slug)
            if prediccion is not None:
                a_ret.append(prediccion)
    if not a_ret:
        return {'message': 'Ningún elemento encontrado'}, 404
    return a_ret
