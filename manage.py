import os

from flask_script import Manager

from anomaly_detection_app import blueprint
from anomaly_detection_app.main import create_app

anomaly_app = create_app(os.getenv('BOILERPLATE_ENV') or 'dev')

anomaly_app.register_blueprint(blueprint)
anomaly_app.app_context().push()
manager = Manager(anomaly_app)


@manager.command
def run():
    anomaly_app.run()


if __name__ == '__main__':
    manager.run()
