# Aplicación de Detección de Anomalías en Convocatorias y Contratos de la DNCP

Esta aplicación es una API Flask que provee servicios de:
- Entrenamiento
- Validación y
- Predicción/Clasificación

De procesos de contratación.

## Estructura del proyecto

Los modelos entrenados ya existentes se encuentra en el directorio `anomaly_detection_app/main/core/trained_models`

Existen en total 4 modelos entrenados:
- (2) Modelos de clasificación usando IsolationForest para detección de anomalías en los procesos
- (2) Modelos de interpretación usando DecisionTree para la interpretación de los resultados obtenidos por el 
modelo de clasificación

Las variables utilizadas por cada modelo están listadas en el archivo `anomaly_detection_app/main/config.py`

Los queries utilizados para la extracción de los datos están en el directorio `anomaly_detection_app/main/core/transform/queries`

En el archivo `anomaly_detection_app/main/core/transform/transform.py` se realizan las transformaciones numéricas a las variables
extraídas con los queries.

En el directorio `anomaly_detection_app/main/resources/scripts_sql` están los scripts iniciales de base de datos

Los servicios implementados junto con su documentación se encuentran en `anomaly_detection_app/main/controller/models_controller.py`

En el directorio `documentacion` se encuentra el código original de la tesis de Elizabeth Keller que fue la base de este
proyecto. El código original también se encuentra disponible [aquí](https://gitlab.com/MaEliK/anomaly_detection_public_procurements_py)

## Deploy

Asegurarse que los scripts iniciales (`anomaly_detection_app/main/resources/scripts_sql`) 
estén ejecutados en la base de datos a utilizar

### Variables de entorno

Las variables de entorno con sus valores de ejemplo son:

    - DB_URL=postgresql://anomalias:anomalias@dbdesa02.dncp.gov.py:5432/db_reinge20200105
    - TRAINING_DATA_PATH=/home/yohanna/desarrollo/anomaly_detection_public_procurements_py/datos_entrenamiento
    - MODELS_DATA_PATH=/home/yohanna/desarrollo/anomaly_detection_public_procurements_py/modelos
    - LOG_PATH=/home/yohanna/desarrollo/anomaly_detection_public_procurements_py/
    
El usuario de base de datos debe ser `anomalias`.

TRAINING_DATA_PATH corresponde a la carpeta en la cual irán los csvs usados para el entrenamiento del modelo en caso
que se reentrene.
MODEL_DATA_PATH corresponde al directorio en donde se almacenarán los modelos entrenados en caso de un reentrenamiento
LOG_PATH donde irán los archivos del log del entrenamiento en background

### Deployar con Docker
Para deployar con docker y docker-compose:
- setear las variables de entorno listadas en docker-compose. Y la dirección de los volúmenes de datos a utilizar.
- Construir la imagen `docker-compose build`
- Iniciar el servicio `docker-compose up -d`
- Visitar la url http://localhost:9000/

### Deployar Localmente
- Tener instalado python en su version 3.7 y virtualenv
- Crear un entorno virtual con `virtualenv -p python3 .ve`
- Activar el entorno source `.ve/bin/activate`
- ejecutar `pip install -r requirements.txt`
- Setear las variables de entorno
- Ejecutar con `uwsgi --ini app.ini`
- Visitar la url http://localhost:9000/