-------------------------------------------------------------------------------
Obtener los datos
-------------------------------------------------------------------------------
1. Obtener datos en forma de record-package de la API proporcionada del DNCP
mediante el script fetch_py, el cual guarda en una base de datos PostgreSQL
creada anteriormente en formato jsonb.

2. Obtener las etiquetas de cada record-package mediante el script
fetch_etiquetas.py, se adjuntan a los datos del record-package con el script
agregar_etiquetas.py.

3. De la misma manera obtener los tenderers de cada record-package mediante el
script fetch_tenderers.py, y adjuntarlos al record-package con add_tenderers.py

4. Obtener las cotizaciones de bcp en forma de excel (descargadas en la carpeta
cotizaciones) y agregarlas a la base de datos con el script cotizaciones_a_DB.py

5. Descargar los datos de la base de datos a una carpeta local a fin de aplicar
el script del cambio de versión a la versión 1.1 del OCDS, utilizar el script
save_to_directory.py

6. Cambiar los datos a la versión 1.1 ejecutando el script update_to_v1_1.py

7. Validando el cambio de los datos mediante el script validate_v1_1.py

-------------------------------------------------------------------------------
Transformar los datos
-------------------------------------------------------------------------------
La transformación de los datos se realiza con 4 conjuntos de datos:

1. Información de planificación y tender, con estados activo y forma de entrega
"written", se ejecuta la función main_tender.py, cambiando en la consulta a la
base de datos el parametro de la forma de entrega y en las líneas 46 y 47 el
nombre de los archivos de salida csv y del diccionario. Si cae el proceso se puede
reanundar de vuelta, y añade a los archivos especificados.

2. Información de planificación y tender, con estados activo y forma de entrega
"electronicAuction", se ejecuta de la misma manera que el punto anterior.

3. Información de planificación, tender, award y contrato, con estado no activo y
forma de entrega "written", se ejecuta la función main_contracts.py, cambiando en la
consulta a la base de datos el parametro de la forma de entrega y en las líneas 61 y 62 el
nombre de los archivos de salida csv y del diccionario. Si cae el proceso se puede
reanundar de vuelta, y añade a los archivos especificados.

4. Información de planificación, tender, award y contrato, con estado no activo y
forma de entrega "electronicAuction", se ejecuta de la misma manera que el punto anterior.

5. Información de contrato y adendas, con estado no activo y forma de entrega "written",
se ejecuta la función main_adendas.py, cambiando en la consulta a la
base de datos el parametro de la forma de entrega y en las líneas 52 y 53 el
nombre de los archivos de salida csv y del diccionario. Si cae el proceso se puede
reanundar de vuelta, y añade a los archivos especificados.

4. Información de contrato y adenda, con estado no activo y forma de entrega
"electronicAuction", se ejecuta de la misma manera que el punto anterior.

Los scripts se encuentran en la carpeta convert_data

-------------------------------------------------------------------------------
Entrenar el modelo
-------------------------------------------------------------------------------
Para entrenar el modelo se utiliza el script scikit_isolation_forest.py en
train_models, se tiene que especificar el archivo con los datos convertidos, el número
de estimadores, el número de muestras y el número máximo de atributos a utilizar. El
script deja como resultado el modelo entrenado en un archivo joblib y los scores resultantes
para el conjunto de entrenamiento.

-------------------------------------------------------------------------------
Validar el modelo
-------------------------------------------------------------------------------
La validación se hace con dos conjuntos de datos conocidos cómo denuncias y protestas.
Se realiza con el script validaciones_protestas_denuncias.py.

## Datos Requeridos para modelos y su equivalencia a datos del sicp
- id -> nro pac
- ocid -> nro pac + prefijo
- planning_etiquettes -> marcas
- planning_budget_amount -> monto_global_estimado - _moneda
- tender_id -> llamado slug
- tenderPeriod_startDateMonth -> fecha_inicio_propuesta
- tenderPeriod_endDateMonth -> fecha_entrega_oferta
- tenderPeriod_maxExtentDateMonth
- tenderPeriod_period
- tenderPeriod_maxExtentPeriod
- tender_amount -> monto_estimado - moneda.codigo
- tender_enquiryPeriod_startDateMonth -> null
- tender_enquiryPeriod_endDateMonth	 -> fecha_tope_consulta
- tender_enquiryPeriod_period	
- tender_awardPeriod_startDateMonth	 -> fecha_tope_consulta
- tender_awardPeriod_endDateMonth	-> fecha_cierre_subasta
- tender_awardPeriod_period	
- tender_hasEnquiries	-> FALSE siempre
- tender_procuringEntity_identifier	-> null
- tender_procuringEntity_contactPoint_email	-> email_contacto
- tender_procuringEntity_memberOf	-> no se usa
- tender_procuringEntity_roles	-> estatico
- tender_procurementMethodDetails	 -> null
- tender_documentTypes -> tipo_documento

Todo eso más los siguientes campos para contract
- tender_numberOfTenderers -> count de numero de oferentes
- award_id -> adjudicacion slug
- award_dateMonth -> fecha_publicacion
- award_amount -> monto_total_adjudicado
- award_status -> siempre 0 
- period_tender_award -> siempre 0
- contract_id -> proveedor adjudicado slug
- contract_startDateMonth -> calculado
- contract_endDateMonth
- contract_period
- award_contractStartDate_period
- contract_dateSignedMonth -> fecha_firma_contrato
- contract_dateSigned_period 
- award_contractSignedDate_period
- contract_amount -> monto_adjudicado
- contract_status -> _estado
- contract_documentTypes -> tipo_documento
- contract_supplier_identifier -> ruc, razon_social
- contract_supplier_contactPoint_email ->correo_electronico
- contract_supplier_address_city -> 
- contract_supplier_memberOf -> 0 
- contract_supplier_roles -> suppliers




 
