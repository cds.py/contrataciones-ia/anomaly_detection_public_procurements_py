from convert_data.convert_string import convertArray
from convert_data.convert_string import appendNameValue


def documents(document, prefix):
    title_row = []
    row = []
    if 'documentType' in document:
        document_type = convertArray(document['documentType'], 'documentType')
        appendNameValue(document_type, prefix + '_documentType', title_row, row, None)
    return [title_row, row]
