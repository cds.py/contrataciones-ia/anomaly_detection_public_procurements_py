from convert_data.getExchangeRate import getExchangeRate
from convert_data.convert_string import convertArray
import convert_data.parties as parties
from datetime import datetime
from convert_data.convert_string import appendNameValue


def contracts(contract, all_parties, title_row, row, moneda_date, award_date, award_suppliers, date):
    contract_id = contract['id']
    print(contract_id)
    contract_award_id = None
    adenda_title_row = []
    adenda_row = []
    contract_extendscontractid = None
    if 'awardID' in contract:
        contract_award_id = contract['awardID']
    if 'extendsContractID' in contract or 'ampliacion' in contract_id or 'reajuste' in contract_id or \
            'modificacion' in contract_id or 'renovacion' in contract_id or "prorroga" in contract_id:
        contract_code_id = contract_id
        appendNameValue(contract_code_id, 'adenda_id', adenda_title_row, adenda_row, None)
        moneda = contract["value"]["currency"]
        amount = contract["value"]["amount"]
        if 'extendsContractID' in contract:
            contract_extendscontractid = contract['extendsContractID']
        appendNameValue(contract_id, 'adenda_contract_id', adenda_title_row, adenda_row, None)
        if 'dateSigned' in contract:
            if contract['dateSigned'] is not None:
                date_signed = datetime.strptime(contract['dateSigned'], '%Y-%m-%dT%H:%M:%SZ')
                month = date_signed.month
                moneda_date = date_signed
                date_signed = date_signed.strftime('%s')
                appendNameValue(int(date_signed), 'adenda_contract_dateSigned', adenda_title_row, adenda_row, None)
                appendNameValue(int(month), 'adenda_contract_dateSignedMonth', adenda_title_row, adenda_row, None)
        if moneda is not None and amount is not None:
            if moneda_date is not None:
                exchange_rate = getExchangeRate(moneda, moneda_date)
                amount = amount * exchange_rate
            appendNameValue(amount, 'adenda_contract_amount', adenda_title_row, adenda_row, None)
            planning_budget_currency = convertArray(moneda, 'currency')
            appendNameValue(planning_budget_currency, 'adenda_contract_currency', adenda_title_row, adenda_row, None)
        if 'dncpAmendmentType' in contract:
            award_status = convertArray(contract['dncpAmendmentType'], 'dncpAmendmentType')
            appendNameValue(award_status, 'adenda_contract_dncpAmendmentType', adenda_title_row, adenda_row, None)
        # if 'status' in contract:
        #     award_status = convertArray(contract['status'], 'status')
        #     appendNameValue(int(award_status), 'adenda_contract_status', title_row, row, None)
        if 'documents' in contract:
            aux_row = []
            for document in contract['documents']:
                if 'documentType' in document:
                    aux_row.append(document['documentType'])
            document_type = convertArray(aux_row, 'contractDocumentType')
            appendNameValue(document_type, 'adenda_documentTypes', adenda_title_row, adenda_row, None)
    elif 'extendsContractID' not in contract and 'ampliacion' not in contract_id and 'reajuste' not in contract_id and 'modificacion' not in contract_id and 'renovacion' not in contract_id:
        contract_code_id = contract_id
        appendNameValue(contract_code_id, 'contract_id', title_row, row, None)
        # contract_dncpdontractdode = hashValue(contract['dncpContractCode'], False, True)
        # appendNameValue(int(contract_dncpdontractdode), 'contract_dncpContractCode', title_row, row, None)
        moneda = contract["value"]["currency"]
        amount = contract["value"]["amount"]
        end_date = None
        start_date = None
        status = None
        if 'status' in contract:
            status = contract['status']
            status = convertArray(status, 'status')
            appendNameValue(int(status), 'contract_status', title_row, row, None)
        if 'period' in contract:
            if 'startDate' in contract['period']:
                if contract['period']['startDate'] is not None:
                    try:
                        start_date = datetime.strptime(contract['period']['startDate'], '%Y-%m-%dT%H:%M:%SZ')
                    except ValueError:
                        start_date = datetime.strptime(contract['period']['startDate'], '%Y-%m-%d')
                    month = start_date.month
                    moneda_date = start_date
                    start_date = start_date.strftime('%s')
                    appendNameValue(int(start_date), 'contract_startDate', title_row, row, None)
                    appendNameValue(int(month), 'contract_startDateMonth', title_row, row, None)
            if 'endDate' in contract['period']:
                if contract['period']['endDate'] is not None:
                    try:
                        end_date = datetime.strptime(contract['period']['endDate'], '%Y-%m-%dT%H:%M:%SZ')
                    except ValueError:
                        end_date = datetime.strptime(contract['period']['endDate'], '%Y-%m-%d')
                    month = end_date.month
                    end_date = end_date.strftime('%s')
                    appendNameValue(int(end_date), 'contract_endDate', title_row, row, None)
                    appendNameValue(int(month), 'contract_endDateMonth', title_row, row, None)
            if type(end_date) is str and type(start_date) is str:
                period = int(end_date) - int(start_date)
                appendNameValue(int(period), 'contract_period', title_row, row, None)
            if type(start_date) is str and type(award_date[contract_award_id].strftime('%s')) is str:
                period = int(start_date) - int(award_date[contract_award_id].strftime('%s'))
                appendNameValue(int(period), 'award_contractStartDate_period', title_row, row, None)
        if 'dateSigned' in contract:
            if contract['dateSigned'] is not None:
                date_signed = datetime.strptime(contract['dateSigned'], '%Y-%m-%dT%H:%M:%SZ')
                month = date_signed.month
                moneda_date = date_signed
                date_signed = date_signed.strftime('%s')
                appendNameValue(int(date_signed), 'contract_dateSigned', title_row, row, None)
                appendNameValue(int(month), 'contract_dateSignedMonth', title_row, row, None)
                if type(date_signed) is str and type(start_date) is str:
                    period = int(date_signed) - int(start_date)
                    appendNameValue(int(period), 'contract_dateSigned_period', title_row, row, None)
                if type(date_signed) is str and type(award_date[contract_award_id].strftime('%s')) is str:
                    period = int(date_signed) - int(award_date[contract_award_id].strftime('%s'))
                    appendNameValue(int(period), 'award_contractSignedDate_period', title_row, row, None)
        if moneda is not None and amount is not None:
            if moneda_date is None:
                if award_date[contract_award_id]:
                    moneda_date = award_date[contract_award_id]
                elif date:
                    date = datetime.strptime(date, '%Y-%m-%dT%H:%M:%SZ')
                    moneda_date = date
            if amount > 0:
                exchange_rate = getExchangeRate(moneda, moneda_date)
                planning_budget_amount = amount * exchange_rate
                appendNameValue(planning_budget_amount, 'contract_amount', title_row, row, None)
                planning_budget_currency = convertArray(moneda, 'currency')
                appendNameValue(planning_budget_currency, 'contract_currency', title_row, row, None)
        if 'documents' in contract:
            aux_row = []
            for document in contract['documents']:
                if 'documentType' in document:
                    aux_row.append(document['documentType'])
            document_type = convertArray(aux_row, 'contractDocumentType')
            appendNameValue(document_type, 'contract_documentTypes', title_row, row, None)
        if 'suppliers' in contract:
            party_id = None
            for party in all_parties:
                if 'identifier' in party:
                    party_identifier = party["identifier"]
                    if party_identifier['id'] == contract['suppliers']['identifier']['id']:
                        party_id = party['id']
            for sup in award_suppliers[contract_award_id]:
                if contract['suppliers']['name'] == sup['name']:
                    party_id = sup['id']
            party = parties.parties_list[party_id]
            for i in party['title_row']:
                title_row.append('contract_supplier_' + i)
            for i in party['row']:
                row.append(i)
    return [contract_id, contract_award_id, title_row, row,
            contract_extendscontractid, adenda_title_row, adenda_row]
