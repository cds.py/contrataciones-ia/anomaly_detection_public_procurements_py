from convert_data.getExchangeRate import getExchangeRate
from convert_data.convert_string import removeDividingCharacters
from convert_data.convert_string import appendNameValue
from convert_data.convert_string import convertArray


def items(item, moneda_date, prefix):
    row = []
    title_row = []
    if 'classification' in item:
        if 'id' in item['classification']:
            removeDividingCharacters(item['classification']['id'])
            appendNameValue(removeDividingCharacters(item['classification']['id']), prefix + '_item_classification',
                            title_row, row, None)

    if 'unit' in item:
        moneda = item['unit']["value"]["currency"]
        amount = item['unit']["value"]["amount"]
        if moneda is not None and amount is not None:
            exchange_rate = getExchangeRate(moneda, moneda_date)
            appendNameValue(amount * exchange_rate, prefix + '_item_amount', title_row, row, None)
            currency = convertArray(moneda, 'currency')
            appendNameValue(currency, prefix + '_item_currency', title_row, row, None)
    if 'quantity' in item:
        if item['quantity']:
            appendNameValue(item['quantity'], prefix + '_item_quantity', title_row, row, None)
    return [title_row, row]
