from anomaly_detection_app.main import util


def get_exchange_rate(moneda, date):
    if moneda == 'USD':
        date = date.date()
        conn = util.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT compra from sicp.cotizacion where fecha = %s;", [str(date)])
        datos = cursor.fetchone()
        cursor.close()
        conn.close()
        return datos[0]
    elif moneda == 'PYG':
        return 1
