import pandas as pd
from joblib import load

from documentacion.obtener_datos.get_and_transform import get_and_transform, get_and_transform_bulk

direccion = 'models/'
filename = 'contratos_written_model_estimators_100_samples_20.joblib'
clf = load(direccion + filename)

data = get_and_transform('224824', True)
data_test3 = data.drop(['llamado_slug', 'id', 'adjudicacion_slug', 'proveedor_adjudicado_slug',
                        'proveedor_roles', 'contrato_estado', 'estado_adjudicacion'], axis=1)
print(data_test3.iloc[0])
prediction2 = clf.predict(data_test3)
# 1 is inlier, -1 outlier
scores2 = clf.decision_function(data_test3)
predictions = pd.DataFrame()
predictions["predicted_class"] = prediction2
predictions["scores"] = scores2
predictions["id"] = id
#predictions["tender_id"] = llamado_id
print(scores2)


get_and_transform_bulk(2010, 2010, False)
