import json
import psycopg2


def change_version():
    from psycopg2.extras import register_json
    register_json(oid=3802, array_oid=3807)
    query = """
        Select id, data from contratos_con_etiqueta_2018;
        """
    counter = 0
    conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
    cur = conn.cursor()
    cur.execute(query)
    contratos = cur.fetchall()
    cur.close()
    del cur
    conn.close()
    del conn
    for row in contratos:
        try:
            data = row[1]
            conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
            cursor = conn.cursor()
            query = "SELECT data " \
                    "from tenderes " \
                    "where id = " + str(row[0]) + ";"
            cursor.execute(query)
            eti = cursor.fetchone()
            cursor.close()
            del cursor
            conn.close()
            del conn
            NoneType = type(None)
            if not isinstance(eti, NoneType):
                tenderers = eti[0]
                num_tenderes = len(tenderers["@graph"][0]["proveedor"]["list"])
                tenderers_list = []
                print(row[0])
                print(num_tenderes)
                for k in range(0, num_tenderes):
                    name = tenderers["@graph"][0]["proveedor"]["list"][k]["razon_social"]
                    if tenderers["@graph"][0]["proveedor"]["list"][k]["ruc"] is not None:
                        ruc = tenderers["@graph"][0]["proveedor"]["list"][k]["ruc"]
                    else:
                        ruc = 'null'
                    tenderer = {}
                    identifier = {"legalName": name, "id": ruc}
                    tenderer["identifier"] = identifier
                    tenderer["name"] = name
                    tenderers_list.append(tenderer)
                data["records"][0]["compiledRelease"]["tender"]["numberOfTenderers"] = num_tenderes
                data["records"][0]["compiledRelease"]["tender"]["tenderers"] = tenderers_list
                counter = counter + 1

            conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
            cursor = conn.cursor()
            query = "INSERT INTO contratos_etiqueta_tenderers_2018 (id, data) " \
                    "VALUES (%s, %s);"
            d = (row[0], json.dumps(data, indent=2, ensure_ascii=False))
            cursor.execute(query, d)
            conn.commit()
            cursor.close()
            del cursor
            conn.close()
            del conn
        except Exception as e:
            print(str(counter) + " tenderers")
            print('Error: ' + str(e))
            if str(e)[:13] != "duplicate key":
                with open("errors-agregar-tenderes-2018.txt", 'a') as err:
                    err.write(str(row[0]) + "\n")
    print(str(counter) + " tenderers")


change_version()
