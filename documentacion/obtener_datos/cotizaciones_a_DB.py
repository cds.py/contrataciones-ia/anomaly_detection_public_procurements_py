import calendar
import os
from datetime import datetime
from urllib.parse import urlparse

import psycopg2
import xlrd

# https://www.bcp.gov.py/cotizacion-minorista-del-tipo-de-cambio-nominal-i368

indir = "cotizaciones/"
PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), indir)


def datos_bcp():
    connection = get_connection()
    for year in range(2010, 2020):
        for mes in range(1, 13):
            print(year, mes)
            inicio, fin = calendar.monthrange(int(year), mes)
            inicio = 1
            mes_actual = '0' + str(mes) if len(str(mes)) < 2 else str(mes)
            file = os.path.join(PATH, str(year), 'Cotizaciones_' + mes_actual + '_' + str(year) + '.xlsx')
            wb = xlrd.open_workbook(file)
            sheet = wb.sheet_by_index(0)
            dolar_column = 0
            dolar_row = 0
            for i in range(sheet.ncols):
                for j in range(sheet.nrows):
                    if str(sheet.cell_value(j, i)).lower() == 'dólar':
                        dolar_column = i
                        dolar_row = j + 2
                        break
            for j in range(dolar_row, sheet.nrows):
                dia = str(sheet.cell_value(j, dolar_column - 1)).replace('.0', '')
                if not dia.isnumeric() or (fin < int(dia) or int(dia) < inicio):
                    break
                compra = sheet.cell_value(j, dolar_column)
                if '-' not in str(compra):
                    venta = sheet.cell_value(j, dolar_column + 1)
                    datetime_object = datetime.strptime('{}-{}-{}'.format(year, mes, dia), '%Y-%m-%d')
                    if not get(connection, datetime_object):
                        print('insertando', datetime_object)
                        insert(connection, datetime_object, compra, venta)
                    else:
                        print('ya existe cotizacion para la fecha', datetime_object)


def get_connection():
    DB_URL = os.getenv('DB_URL', 'postgresql://anomalias:anomalias@dbdesa02.dncp.gov.py:5432/db_reinge20200105')
    result = urlparse(DB_URL)
    username = result.username
    password = result.password
    database = result.path[1:]
    hostname = result.hostname
    connection = psycopg2.connect(
        database=database,
        user=username,
        password=password,
        host=hostname
    )
    return connection


def insert(conn, fecha, compra, venta):
    cursor = conn.cursor()
    cursor.execute("INSERT INTO sicp.cotizacion (fecha, compra, venta) VALUES(%s, %s, %s)", (fecha, compra, venta))
    conn.commit()
    cursor.close()


def get(conn, fecha):
    fecha = fecha.date()
    cursor = conn.cursor()
    cursor.execute('SELECT compra from sicp.cotizacion where fecha = %s;', [str(fecha)])
    data = cursor.fetchone()
    cursor.close()
    return data


datos_bcp()
