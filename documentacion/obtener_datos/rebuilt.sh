#!/bin/sh

sudo rm -R ~/Documents/contratos_change_version/all
sudo rm -R ~/Documents/contratos_change_version/a_validar
sudo rm -R ~/Documents/contratos_change_version/a_validar_versioned
sudo rm -R ~/Documents/contratos_change_version/no_existe_ofertantes
sudo rm -R ~/Documents/contratos_change_version/no_existe_ofertantes_versioned
sudo rm -R ~/Documents/contratos_change_version/no_tender
sudo rm -R ~/Documents/contratos_change_version/no_tender_versioned
sudo rm -R ~/Documents/contratos_change_version/something_wrong
sudo rm -R ~/Documents/contratos_change_version/prueba

mkdir ~/Documents/contratos_change_version/all
mkdir ~/Documents/contratos_change_version/a_validar
mkdir ~/Documents/contratos_change_version/a_validar_versioned
mkdir ~/Documents/contratos_change_version/no_existe_ofertantes
mkdir ~/Documents/contratos_change_version/no_existe_ofertantes_versioned
mkdir ~/Documents/contratos_change_version/no_tender
mkdir ~/Documents/contratos_change_version/no_tender_versioned
mkdir ~/Documents/contratos_change_version/something_wrong
mkdir ~/Documents/contratos_change_version/prueba

sudo mv ~/Documents/contratos_change_version/all.json ~/Documents/contratos_change_version/all/
cd ~/Documents/contratos_change_version/all
split --lines=1 all.json
sudo mv ~/Documents/contratos_change_version/all/all.json ~/Documents/contratos_change_version/
